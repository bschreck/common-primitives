import numpy as np
from functools import reduce

from common_primitives.csv_feature_extractor import CSVFeatureExtractor, Hyperparams as CSV_HP
from common_primitives.one_hot_maker import OneHotMaker, Hyperparams as OH_HP
from common_primitives.pca import PCA, Hyperparams as PCA_HP

"""
  Input
    |
CSVFeatureExtractor
    |
OneHotMaker
    |
   PCA
    |
  Output
"""


class RegularisedPCAPipeline():

    def __init__(self):
        """ Create an instance of each primitive """
        self.csv_extractor = CSVFeatureExtractor(hyperparams=CSV_HP.defaults())
        self.vectorizer = OneHotMaker(hyperparams=OH_HP.defaults())
        hy = PCA_HP.defaults()
        hy1 = dict(hy)
        hy1['proportion_variance'] = 0.9
        hy2 = PCA_HP(hy1)
        self.pca = PCA(hyperparams=hy2)

    def train(self, x_file, y_file, iterations):
        """
        Train the pipeline for each primitive, first train the primitive on the
        data and then produce output from it. Train each primitive on the
        previous one's output.
        """
        extracted_x = self.csv_extractor.produce(inputs=[x_file]).value
        extracted_y = self.csv_extractor.produce(inputs=[y_file]).value
        extracted_x.pop("d3mIndex")
        extracted_x.pop("idnum")
        extracted_y.pop("d3mIndex")

        self.vectorizer.set_training_data(inputs=extracted_x)
        self.vectorizer.fit()
        embedded_x = self.vectorizer.produce(inputs=extracted_x)
        embedded_y = np.array(next(iter(extracted_y.values)))

        self.pca.set_training_data(inputs=embedded_x)
        self.pca.fit()
        prin_comp_x = self.pca.produce(inputs=embedded_x)

        n_components = prin_comp_x.shape[1]

    def make_predictions(self, x_file):
        extracted_x = self.csv_extractor.produce(inputs=[x_file]).value
        extracted_x.pop("d3mIndex")
        extracted_x.pop("idnum")

        embedded_x = self.vectorizer.produce(inputs=extracted_x)

        prin_comp_x = self.pca.produce(inputs=embedded_x)
        return prin_comp_x

    def RMSE(self, test_x_file, test_y_file):
        predictions = self.make_predictions(test_x_file)

        targets = self.csv_extractor.produce(inputs=[test_y_file]).value
        targets.pop("d3mIndex")
        targets = np.array(next(iter(targets.values)))

        # Return Mean Squared Error
        return np.sqrt(np.mean((predictions-targets)**2))


# Get data
training_x_file = "data/radon_train.csv"
training_y_file = "data/trainTargets.csv"

pipeline = RegularisedPCAPipeline()
pipeline.train(training_x_file, training_y_file, iterations=10)    # Ideally run for > 100 but takes a while

print("RMSE:", pipeline.RMSE(training_x_file, training_y_file))
# print(pipeline.linreg.get_params())
