import os
import re
import sys
from setuptools import setup, find_packages

PACKAGE_NAME = 'common_primitives'
MINIMUM_PYTHON_VERSION = 3, 6

def check_python_version():
    """Exit when the Python version is too low."""
    if sys.version_info < MINIMUM_PYTHON_VERSION:
        sys.exit("Python {}.{}+ is required.".format(*MINIMUM_PYTHON_VERSION))


def read_package_variable(key):
    """Read the value of a variable from the package without importing."""
    module_path = os.path.join(PACKAGE_NAME, '__init__.py')
    with open(module_path) as module:
        for line in module:
            parts = line.strip().split(' ')
            if parts and parts[0] == key:
                return parts[-1].strip("'")
    assert False, "'{0}' not found in '{1}'".format(key, module_path)

check_python_version()
version = read_package_variable('__version__')

setup(
    name=PACKAGE_NAME,
    version=version,
    description='D3M common primitives',
    author=read_package_variable('__author__'),
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    install_requires=[
        'd3m_metadata',
        'primitive_interfaces',
        'scikit-learn',
        'numpy',
        'pymc3',
        'torch',
        'torchvision'
    ],
    url='https://gitlab.com/datadrivendiscovery/common-primitives',
    entry_points = {
        'd3m.primitives': [
            'common_primitives.BayesianLogisticRegression = common_primitives.logistic_regression:BayesianLogisticRegression',
            'common_primitives.ConvolutionalNeuralNet = common_primitives.convolutional_neural_net:ConvolutionalNeuralNet',
            'common_primitives.CSVFeatureExtractor = common_primitives.csv_feature_extractor:CSVFeatureExtractor',
            'common_primitives.DiagonalMVN = common_primitives.diagonal_mvn:DiagonalMVN',
            'common_primitives.FeedForwardNeuralNet = common_primitives.feed_forward_neural_net:FeedForwardNeuralNet',
            'common_primitives.ImageReader = common_primitives.image_reader:ImageReader',
            'common_primitives.KMeans = common_primitives.k_means:KMeans',
            'common_primitives.LinearRegression = common_primitives.linear_regression:LinearRegression',
            'common_primitives.Loss = common_primitives.loss:Loss',
            'common_primitives.OneHotMaker = common_primitives.one_hot_maker:OneHotMaker',
            'common_primitives.PCA = common_primitives.pca:PCA',
            'common_primitives.RandomForestClassifier = common_primitives.random_forest:RandomForestClassifier'
        ],
    },
    dependency_links=[
        'git+https://gitlab.com/datadrivendiscovery/metadata.git@devel#egg=d3m_metadata-{version}'.format(version=version),
        'git+https://gitlab.com/datadrivendiscovery/primitive-interfaces.git@devel#egg=d3m_metadata-{version}'.format(version=version),
        'https://download.pytorch.org/whl/cpu/torch-0.3.0.post4-cp36-cp36m-linux_x86_64.whl'
    ],

)
