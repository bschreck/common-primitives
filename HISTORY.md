## vNEXT

* Created `devel` branch for integration branch.
* Renamed repository and package to `common-primitives` and `common_primitives`,
  respectively.
* Repository migrated to gitlab.com and made public.
* Added `Ridge`, `LinearSVC`, `LassoCV`, `KNeighborsClassifier`, `DecisionTree`
  `BayesianRidge` primitives.
* Added an example pipeline with end-to-end training.

## v0.1.1

* Made example primitives work on Python 2.7.

## v0.1.0

* Initial set of example primitives.
