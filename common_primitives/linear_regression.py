import time
import os

from typing import NamedTuple, Sequence, Any, List, Dict, Union, Tuple
from d3m_metadata.container.list import List
from d3m_metadata.container.numpy import ndarray
import torch  # type: ignore
import numpy as np  # type: ignore
from common_primitives.diagonal_mvn import DiagonalMVN, Hyperparams as MVNHyperparams

from d3m_metadata import hyperparams, params, metadata as metadata_module, utils
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from primitive_interfaces.base import ProbabilisticCompositionalityMixin, \
                                      GradientCompositionalityMixin, \
                                      SamplingCompositionalityMixin, \
                                      ContinueFitMixin, \
                                      CallResult, \
                                      Gradients, Scores


from .utils import to_variable, refresh_node, log_mvn_likelihood

# These are just regular Python variables so that we can easily change all types
# at once in the future, if needed. Otherwise, one could simply inline all these.
# 2D array of n_inputs x n_dimensions
Inputs = ndarray
# 1D array of floats of length n_inputs
Outputs = ndarray

# A named tuple for parameters.
# Specifying types for all parameters is important so that one can do end-to-end
# training from outside. For example, some parameters might have gradients so we
# can use those to optimize them end-to-end.
class Hyperparams(hyperparams.Hyperparams):
    alpha = hyperparams.Hyperparameter[float](
                            default=1e-3,
                            description='initial fitting step size'
                            )
    beta = hyperparams.Hyperparameter[float](
                            default=1e-8,
                            description='see reference for details'
                            )
    weights_prior = hyperparams.Hyperparameter[GradientCompositionalityMixin](
                            default=DiagonalMVN(hyperparams=MVNHyperparams.defaults()),
                            description='prior on weights'
                            )
    noise_prior = hyperparams.Hyperparameter[GradientCompositionalityMixin](
                            default=DiagonalMVN(hyperparams=MVNHyperparams.defaults()),
                            description='prior on noise'
                            )
    offset_prior = hyperparams.Hyperparameter[GradientCompositionalityMixin](
                            default=DiagonalMVN(hyperparams=MVNHyperparams.defaults()),
                            description='prior on offset'
                            )


class Params(params.Params):
    weights: ndarray
    offset: float
    noise_variance: float

class LinearRegression(ProbabilisticCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                       GradientCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                       SamplingCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                       ContinueFitMixin[Inputs, Outputs, Params, Hyperparams],
                       # Base class should be after all mixins.
                       SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    Example of a primitive wrapping implementing a linear regression using a high-performance
    PyTorch backend, providing methods from multiple mixins.
    """

    __author__ = "Oxford DARPA D3M Team, William Harvey <willh@robots.ox.ac.uk>"
    metadata = metadata_module.PrimitiveMetadata({
         'id': '28ad180e-ef88-427a-ac00-0673efc72b3b',
         'version': '0.1.0',
         'name': 'Bayesian linear regression',
         'keywords': ['Bayesian', 'regression'],
         'source': {
            'name': 'common-primitives',
            'contact' : 'mailto:willh@robots.ox.ac.uk'
         },
         'installation': [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(git_commit=utils.current_git_commit(os.path.dirname(__file__)))
         }],
         'python_path': 'd3m.primitives.common_primitives.LinearRegression',
         'algorithm_types': ['LINEAR_REGRESSION'],
         'primitive_family': 'OPERATOR',
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, str] = None) -> None:
        """
        Optionally set ``alpha``, the initial fitting step size,
        and ``beta``, which is as described in Baydin, Atilim Gunes, et al.,
        'Online Learning Rate Adaptation with Hypergradient Descent',
        arXiv preprint arXiv:1703.04782 (2017).
        """

        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)

        self._alpha = self._initial_alpha = hyperparams['alpha']
        self._beta = hyperparams['beta']
        self._weights_prior = hyperparams['weights_prior']
        try:
            self._weights_prior.get_params()
        except:
            self._weights_prior = None

        self._noise_prior = hyperparams['noise_prior']
        try:
            self._noise_prior.get_params()
        except:
            self._noise_prior = None

        self._offset_prior = hyperparams['offset_prior']
        try:
            self._offset_prior.get_params()
        except:
            self._offset_prior = None

        self._fit_term_temperature = 0.0
        self._offset = None  # type: torch.autograd.Variable
        self._weights = None  # type: torch.autograd.Variable
        self._noise_variance = None  # type: torch.autograd.Variable
        self._training_inputs = None  # type: torch.autograd.Variable
        self._training_outputs = None  # type: torch.autograd.Variable
        self._iterations_done = None  # type: int
        self._has_finished = True
        self._new_training_data = True
        self._inputs = None
        self._outputs = None

    def backward(self, *, gradient_outputs: Gradients[Outputs], fine_tune: bool = False, fine_tune_learning_rate: float = 0.00001,
                 fine_tune_weight_decay: float = 0.00001) -> Tuple[Gradients[Inputs], Scores[Params]]:  # type: ignore
        if self._inputs is None:
            raise Exception('Cannot call backpropagation before forward propagation. Call "produce" before "backprop".')
        else:
            if self._inputs.grad is not None:
                self._inputs.grad.data.zero_()

            # these should be from the most recent training attempt if any
            if self._weights.grad is not None:
                prev_weights_grad = self._weights.grad.data
                self._weights.grad.data.zero_()

            if self._offset.grad is not None:
                prev_offset_grad = self._offset.grad.data
                self._offset.grad.data.zero_()

            if self._noise_variance.grad is not None:
                prev_noise_grad = self._noise_variance.grad.data
                self._noise_variance.grad.data.zero_()

            self._outputs.backward(gradient=to_variable(gradient_outputs))
            weights_grad = self._weights.grad.data
            offset_grad = self._offset.grad.data
            noise_grad = self._noise_variance.grad.data

            if fine_tune:
                if self._weights_prior is not None:
                    weights_grad += torch.from_numpy(self._weights_prior.gradient_output(outputs=np.array([self._weights.data.numpy()]), inputs=[]))

                if self._offset_prior is not None:
                    offset_grad += torch.from_numpy(self._offset_prior.gradient_output(outputs=[self._offset.data.numpy()], inputs=[]))

                if self._noise_prior is not None:
                    noise_grad += torch.from_numpy(self._noise_prior.gradient_output(outputs=[self._noise_variance.data.numpy()], inputs=[]))

                if prev_weights_grad is not None:
                    self._alpha += self._beta * (torch.dot(weights_grad, prev_weights_grad)
                                               + torch.dot(noise_grad, prev_noise_grad)
                                               + torch.dot(offset_grad, prev_offset_grad))


                self._weights.data += weights_grad * self._alpha / torch.norm(weights_grad)
                self._offset.data += offset_grad * self._alpha / torch.norm(offset_grad)
                self._noise_variance.data += noise_grad * self._alpha / torch.norm(noise_grad)

                self._weights = refresh_node(self._weights)
                self._offset = refresh_node(self._offset)
                self._noise_variance = refresh_node(self._noise_variance)

            grad_inputs = self._inputs.grad

            grad_params = Params(weights=weights_grad, offset=offset_grad, noise_variance=noise_grad)
            return grad_inputs, grad_params


    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self._training_inputs = to_variable(inputs, requires_grad=True)
        self._training_outputs = to_variable(outputs, requires_grad=True)
        self._new_training_data = True


    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        """
        inputs : num_inputs x D numpy array
        outputs : numpy array of dimension (num_inputs)
        """

        self._weights = refresh_node(self._weights)
        self._offset = refresh_node(self._offset)
        self._noise_variance = refresh_node(self._noise_variance)

        self._inputs = to_variable(inputs, requires_grad=True)
        self._iterations_done = None
        self._has_finished = True
        self._outputs = self._offset + torch.mm(self._inputs, self._weights.unsqueeze(0).transpose(0, 1)).squeeze()

        return CallResult(self._outputs.data.numpy(), has_finished = self._has_finished, iterations_done=self._iterations_done)

    def continue_fit(self, *, timeout: float = None, iterations: int = None) -> None:
        fit_threshold = 0
        batch_size = None
        """
        Starting from previous parameters, runs gradient descent for ``timeout``
        seconds or ``iterations`` iterations, whichever comes sooner, on log
        normal_density(self.weights * self.input - output, identity*self.noise_variance) +
        parameter_prior_primitives["weights"].score(self.weights) +
        parameter_prior_primitives["noise_variance"].score(noise_variance).
        """
        if self._training_inputs is None or self._training_outputs is None:
            raise ValueError("Missing training data.")
        return self._fit(timeout=timeout,
                         iterations=iterations,
                         fit_threshold=fit_threshold,
                         batch_size=batch_size)

    def fit(self, *, timeout: float = None, iterations: int = None, fit_threshold: float, batch_size: int) -> CallResult:
        """
        Runs gradient descent for ``timeout`` seconds or ``iterations``
        iterations, whichever comes sooner, on log normal_density(self.weights * self.input
        - output, identity*self.noise_variance) +
        parameter_prior_primitives["weights"].score(self.weights) +
        parameter_prior_primitives["noise_variance"].score(noise_variance).
        """
        if self._new_training_data:
            self._weights = torch.autograd.Variable(torch.FloatTensor(np.random.randn(self._training_inputs.size()[1]) * 0.001),
                                                   requires_grad=True)
            self._noise_variance = torch.autograd.Variable(torch.ones(1),
                                                          requires_grad=True)
            self._offset = torch.autograd.Variable(torch.zeros(1),
                                                  requires_grad=True)
            self._new_training_data = False
            self._alpha = self._initial_alpha
        self._fit(timeout=timeout, iterations=iterations, batch_size=batch_size)
        return CallResult(None, has_finished=self._has_finished, iterations_done=self._iterations_done)

    # Example of a fit method which can be iteratively called multiple times.
    # It also accept additional primitive as an additional argument.
    def _fit(self, *, timeout: float = None, iterations: int = 100, fit_threshold: float = 0, batch_size: int) -> None:

        if self._training_inputs is None or self._training_outputs is None:
            raise ValueError("Missing training data.")

        if timeout is None:
            timeout = np.inf

        if batch_size is None:
            x_batches = [self._training_inputs]
            y_batches = [self._training_outputs]
        else:
            x_batches = []
            y_batches = []
            for i in range(0, len(self._training_inputs), batch_size):
                x_batches.append(self._training_inputs[i:i+batch_size])
                y_batches.append(self._training_outputs[i:i+batch_size])
        num_batches = len(x_batches)

        start = time.time()
        self._iterations_done = 0
        # We can always iterate more, even if not reasonable.
        self._has_finished = False
        prev_weights_grad, prev_noise_grad, prev_offset_grad = None, None, None
        while time.time() < start + timeout and ((iterations is None and self._alpha >= fit_threshold) or (self._iterations_done < iterations)):
            self._iterations_done += 1
            batch_no = self._iterations_done % num_batches

            grads = [self._gradient_params_log_likelihood(input=training_input,
                                                          output=training_output)
                     for training_input, training_output
                     in zip(x_batches[batch_no], y_batches[batch_no])]
            weights_grad = sum(grad[0] for grad in grads) * num_batches
            offset_grad = sum(grad[1] for grad in grads) * num_batches
            noise_grad = sum(grad[2] for grad in grads) * num_batches
            if self._weights_prior is not None:
                weights_grad += torch.from_numpy(self._weights_prior.gradient_output(outputs=np.array([self._weights.data.numpy()]), inputs=[]))

            if self._offset_prior is not None:
                prev_offset_grad += torch.from_numpy(self._offset_prior.gradient_output(outputs=[self._offset.data.numpy()], inputs=[]))

            if self._noise_prior is not None:
                noise_grad += torch.from_numpy(self._noise_prior.gradient_output(outputs=[self._noise_variance.data.numpy()], inputs=[]))

            if prev_weights_grad is not None:
                self._alpha += self._beta * (torch.dot(weights_grad, prev_weights_grad)
                                           + torch.dot(noise_grad, prev_noise_grad)
                                           + torch.dot(offset_grad, prev_offset_grad))

            prev_weights_grad = weights_grad
            prev_offset_grad = offset_grad
            prev_noise_grad = noise_grad

            self._weights.data += weights_grad * self._alpha / torch.norm(weights_grad)
            self._offset.data += offset_grad * self._alpha / torch.norm(offset_grad)
            self._noise_variance.data += noise_grad * self._alpha / torch.norm(noise_grad)

            self._weights = refresh_node(self._weights)
            self._offset = refresh_node(self._offset)
            self._noise_variance = refresh_node(self._noise_variance)

    def get_params(self) -> Params:
        return Params(weights=ndarray(self._weights.data.numpy()), offset=float(self._offset.data.numpy()[0]), noise_variance=float(self._noise_variance.data.numpy()[0]))

    def set_params(self, *, params: Params) -> None:
        self._weights = to_variable(params['weights'], requires_grad=True)
        self._weights.retain_grad()
        self._noise_variance = to_variable(params['noise_variance'], requires_grad=True)
        self._offset = to_variable(params['offset'], requires_grad=True)

    def log_likelihoods(self, *, outputs: Outputs, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[ndarray]:
        """
        input : D-length numpy ndarray
        output : float
        Calculates
        log(normal_density(self.weights * self.input - output, identity * self.noise_variance))
        for a single input/output pair.
        """
        result = np.array([self._log_likelihood(output=to_variable(output),
                                              input=to_variable(input)).data.numpy()
                            for input, output in zip(inputs, outputs)])
        return CallResult(result)

    def log_likelihood(self, *, outputs: Outputs, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[float]:
        result = self.log_likelihoods(outputs=outputs, inputs=inputs, timeout=timeout, iterations=iterations)

        return CallResult(sum(result.value), has_finished=result.has_finished, iterations_done=result.iterations_done)

    def _log_likelihood(self, output: torch.autograd.Variable,
                        input: torch.autograd.Variable) -> \
            torch.autograd.Variable:
        """
        All inputs are torch tensors (or variables if grad desired).
        input : D-length torch to_variable
        output : float
        """
        expected_output = torch.dot(self._weights, input) + self._offset
        covariance = to_variable(self._noise_variance).view(1, 1)
        return log_mvn_likelihood(expected_output, covariance, output)

    def _gradient_params_log_likelihood(self, *, output: torch.autograd.Variable, input: torch.autograd.Variable) -> Tuple[torch.autograd.Variable, torch.autograd.Variable, torch.autograd.Variable]:
        """
        Output is ( D-length torch variable, 1-length torch variable )
        """

        self._weights = refresh_node(self._weights)
        self._noise_variance = refresh_node(self._noise_variance)
        log_likelihood = self._log_likelihood(output=output, input=input)
        log_likelihood.backward()
        return (self._weights.grad.data, self._offset.grad.data, self._noise_variance.grad.data)

    def _gradient_output_log_likelihood(self, *, output: ndarray, input: torch.autograd.Variable) -> torch.autograd.Variable:
        """
        output is D-length torch variable
        """

        output_var = to_variable(output)
        log_likelihood = self._log_likelihood(output=output_var, input=input)
        log_likelihood.backward()
        return output_var.grad

    def gradient_output(self, *, outputs: Outputs, inputs: Inputs) -> Gradients[Outputs]: # type: ignore
        """
        Calculates grad_output fit_term_temperature *
        log normal_density(self.weights * self.input - output, identity * self.noise_variance)
        for a single input/output pair.
        """

        outputs_vars = [to_variable(output, requires_grad=True) for output in outputs]
        inputs_vars = [to_variable(input) for input in inputs]
        grad = sum(self._gradient_output_log_likelihood(output=output,
                                                        input=input)
                   for (input, output) in zip(inputs_vars, outputs_vars))

        if self._fit_term_temperature != 0:
            grad += self._fit_term_temperature * \
                sum(self._gradient_output_log_likelihood(output=training_output,
                                                         input=training_input)
                    for (training_output, training_input) in
                    zip(self._training_outputs, self._training_inputs))

        return grad.data.numpy()

    def gradient_params(self, *, outputs: Outputs, inputs: Inputs) -> Scores[Params]:  # type: ignore
        """
        Calculates grad_weights fit_term_temperature *
        log normal_density(self.weights * self.input - output, identity * self.noise_variance)
        for a single input/output pair.
        """

        outputs_vars = [to_variable(output, requires_grad=True) for output in outputs]
        inputs_vars = [to_variable(input) for input in inputs]

        grads = [self._gradient_params_log_likelihood(output=output, input=input)
                 for (input, output) in zip(inputs_vars, outputs_vars)]
        grad_weights = sum(grad[0] for grad in grads)
        grad_offset = sum(grad[1] for grad in grads)
        grad_noise_variance = sum(grad[2] for grad in grads)

        if self._fit_term_temperature != 0:
            training_grads = [self._gradient_params_log_likelihood(output=output, input=input)
                              for (input, output) in zip(self._training_inputs,
                                                         self._training_outputs)]
            grad_weights += self._fit_term_temperature * \
                sum(grad[0] for grad in training_grads)
            grad_offset += self._fit_term_temperature * \
                sum(grad[1] for grad in training_grads)
            grad_noise_variance += self._fit_term_temperature * \
                sum(grad[2] for grad in training_grads)

        return Params(weights=grad_weights, offset=grad_offset, noise_variance=grad_noise_variance)

    def set_fit_term_temperature(self, *, temperature: float = 0) -> None:
        self._fit_term_temperature = temperature

    def _sample_once(self, *, inputs: Inputs) -> Outputs:
        """
        input : NxD numpy ndarray
        outputs : N-length numpy ndarray
        """
        if self._weights is None or self._noise_variance is None:
            raise ValueError("Params not set.")

        output_means = [np.dot(self._weights.data.numpy(), input)
                        + self._offset.data.numpy()
                        for input in inputs]
        outputs = np.random.normal(output_means,
                                   self._noise_variance.data[0])
        return outputs

    def sample(self, *, inputs: Inputs, num_samples: int = 1, timeout: float = None, iterations: int = None) -> Sequence[Outputs]:
        """
        input : num_inputs x D numpy ndarray
        outputs : num_predictions x num_inputs numpy ndarray
        """

        return [self._sample_once(inputs=inputs) for _ in range(num_samples)]

    def get_call_metadata(self) -> CallResult:
        return CallResult(None, has_finished=self._has_finished, iterations_done=self._iterations_done)
