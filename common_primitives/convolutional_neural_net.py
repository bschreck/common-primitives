
from d3m_metadata.container.numpy import ndarray
from d3m_metadata import hyperparams, params, metadata as metadata_module, utils
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from primitive_interfaces.base import CallResult, GradientCompositionalityMixin, Gradients, Scores

from .utils import to_variable
from typing import Dict, List, Tuple, Type
from functools import reduce
from operator import mul
import os
import abc
import time
import numpy as np  # type: ignore
import torch  # type: ignore
import torch.nn as nn  # type: ignore
import torch.nn.functional as F  # type: ignore
import torch.optim as optim  # type: ignore
from torch.autograd import Variable  # type: ignore

Inputs = ndarray
Outputs = ndarray


class Params(params.Params):
    state: Dict


class Hyperparams(hyperparams.Hyperparams):
    loss_type = hyperparams.Enumeration[str](
        values=['mse', 'crossentropy'],
        default='mse',
        description='Type of loss used for the local training (fit) of this primitive.'
    )
    input_shape = hyperparams.Hyperparameter[List[int]](
        default=[32,32],
        description='Shape of the input.'
    )
    output_dim = hyperparams.Hyperparameter[int](
        default=1,
        description='Dimensions the output.'
    )
    linear_output = hyperparams.Hyperparameter[bool](
        default=False,
        description='Setting determining whether the last (output) layer will have a non-linearity.'
    )
    family = hyperparams.Enumeration[str](
        values=['lenet'],
        default='lenet',
        description='Type of convolutional neural network.'
    )
    optimizer_type = hyperparams.Enumeration[str](
        values=['adam', 'sgd'],
        default='adam',
        description='Type of optimizer used during training (fit).'
    )
    minibatch_size = hyperparams.Hyperparameter[int](
        default=64,
        description='Minibatch size used during training (fit).'
    )
    learning_rate = hyperparams.Hyperparameter[float](
        default=0.00001,
        description='Learning rate used during training (fit).'
    )
    momentum = hyperparams.Hyperparameter[float](
        default=0.9,
        description='Momentum used during training (fit), only for optimizer_type sgd.'
    )
    weight_decay = hyperparams.Hyperparameter[float](
        default=0.00001,
        description='Weight decay (L2 regularization) used during training (fit).'
    )
    shuffle = hyperparams.Hyperparameter[bool](
        default=True,
        description='Shuffle minibatches in each epoch of training (fit).'
    )
    fit_threshold = hyperparams.Hyperparameter[float](
        default = 1e-5,
        description='Threshold of loss value to early stop training (fit).'
    )

class ConvolutionalNeuralNet(GradientCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                             SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    A convolutional neural network primitive using PyTorch.
    """
    __author__ = 'Oxford DARPA D3M Team, Atilim Gunes Baydin <gunes@robots.ox.ac.uk>'
    metadata = metadata_module.PrimitiveMetadata({
         'id': 'f254fda2-bab3-42b4-a09d-1ccd1877327e',
         'version': '0.1.0',
         'name': 'Convolutional neural network primitive using PyTorch',
         'keywords': ['convolutional neural network', 'deep learning'],
         'source': {
            'name': 'common-primitives',
            'contact' : 'mailto:gunes@robots.ox.ac.uk'
         },
         'installation': [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(git_commit=utils.current_git_commit(os.path.dirname(__file__)))
         }],
         'python_path': 'd3m.primitives.common_primitives.ConvolutionalNeuralNet',
         'algorithm_types': ['NEURAL_NETWORK_BACKPROPAGATION'],
         'primitive_family': 'REGRESSION',
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, str] = None) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)

        torch.manual_seed(random_seed)
        self._net = None  # type: Type[torch.nn.Module]
        loss_type = self.hyperparams['loss_type']
        if loss_type == 'mse':
            self._criterion = nn.MSELoss()
            self._linear_output = self.hyperparams['linear_output']
        elif loss_type == 'crossentropy':
            self._criterion = nn.CrossEntropyLoss()
            self._linear_output = True
        else:
            raise ValueError('Unsupported loss_type: {}. Available options: mse, crossentropy'.format(loss_type))
        self._input_shape = self.hyperparams['input_shape']
        self._output_dim = self.hyperparams['output_dim']
        self._family = self.hyperparams['family']
        self._optimizer_type = self.hyperparams['optimizer_type']
        self._minibatch_size = self.hyperparams['minibatch_size']
        self._learning_rate = self.hyperparams['learning_rate']
        self._momentum = self.hyperparams['momentum']
        self._weight_decay = self.hyperparams['weight_decay']
        self._shuffle = self.hyperparams['shuffle']
        self._fit_threshold = self.hyperparams['fit_threshold']

        self._inputs = None  # type: Type[torch.autograd.Variable]
        self._outputs = None  # type: Type[torch.autograd.Variable]
        self._training_inputs = None  # type: Type[torch.autograd.Variable]
        self._training_outputs = None  # type: Type[torch.autograd.Variable]
        self._training_size = 0
        self._iterations_done = 0
        self._has_finished = True
        self._init_net()

    def _create_lenet(self) -> Type[torch.nn.Module]:
        class _Net(nn.Module):
            def __init__(self, input_shape, output_dim, linear_output):
                super().__init__()
                self._input_shape = input_shape
                self._output_dim = output_dim
                self._linear_output = linear_output
                if len(self._input_shape) == 2:
                    self._input_channels = 1
                else:
                    self._input_channels = self._input_shape[0]
                self.conv1 = nn.Conv2d(self._input_channels, 64, kernel_size=3)
                self.conv2 = nn.Conv2d(64, 128, kernel_size=3)

            def configure(self):
                input_sample = to_variable(torch.zeros(self._input_shape).unsqueeze(0))
                self._cnn_output_dim = self._forward_cnn(input_sample).view(-1).size(0)
                self.fc1 = nn.Linear(self._cnn_output_dim, 50)
                self.fc2 = nn.Linear(50, self._output_dim)

            def _forward_cnn(self, x):
                if x.dim() == 3:
                    x = x.unsqueeze(1)
                x = F.relu(F.max_pool2d(self.conv1(x), 2))
                x = F.relu(F.max_pool2d(self.conv2(x), 2))
                return x

            def forward(self, x):
                x = self._forward_cnn(x)
                x = x.view(-1, self._cnn_output_dim)
                x = F.relu(self.fc1(x))
                x = F.dropout(x)
                x = self.fc2(x)
                if not self._linear_output:
                    x = F.relu(x)
                return x

        net = _Net(self._input_shape, self._output_dim, self._linear_output)
        net.configure()
        return net

    def _init_net(self) -> None:
        family = self.hyperparams['family']
        if family == 'lenet':
            self._net = self._create_lenet()
        else:
            raise ValueError('Unsupported family: {}. Available options: lenet'.format(family))

    # TODO: check whether all elements in inputs and outputs sequences have the same shape
    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        if len(inputs) != len(outputs):
            raise ValueError('Training data sequences "inputs" and "outputs" should have the same length.')
        self._training_size = len(inputs)
        self._training_inputs = to_variable(inputs)
        self._training_outputs = to_variable(outputs)
        if self._training_inputs.dim() == 1:
            self._training_inputs = self._training_inputs.unsqueeze(0)
        self._input_shape = self._training_inputs[0].data.numpy().shape
        if isinstance(self._criterion, nn.CrossEntropyLoss):
            self._training_outputs = self._training_outputs.type(torch.LongTensor)
        else:
            if self._training_outputs.dim() == 1:
                self._training_outputs = self._training_outputs.unsqueeze(1)
            if self._output_dim == 1:
                self._output_dim = len(self._training_outputs[0].view(-1))
        self._init_net()

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        if self._net is None:
            raise Exception('Neural network not initialized. You need to set training data so that the network structure can be defined.')
        self._iterations_done = 0
        self._has_finished = True
        self._net.eval()
        self._inputs = to_variable(inputs, requires_grad=True)
        self._outputs = self._net.forward(self._inputs)
        return CallResult(self._outputs.data.numpy())

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        if self._training_inputs is None:
            raise Exception('Cannot fit when no training data is present.')

        if timeout is None:
            timeout = np.inf
        if iterations is None:
            iterations = 100

        if self._minibatch_size > self._training_size:
            self._minibatch_size = self._training_size

        if self._optimizer_type == 'adam':
            optimizer_instance = optim.Adam(self._net.parameters(), lr=self._learning_rate, weight_decay=self._weight_decay)
        elif self._optimizer_type == 'sgd':
            optimizer_instance = optim.SGD(self._net.parameters(), lr=self._learning_rate, momentum=self._momentum, weight_decay=self._weight_decay)
        else:
            raise ValueError('Unsupported optimizer_type: {}. Available options: adam, sgd'.format(self._optimizer_type))

        start = time.time()
        self._iterations_done = 0
        # We can always iterate more, even if not reasonable.
        self._has_finished = False
        self._net.train()
        while time.time() < start + timeout and self._iterations_done < iterations:
            self._iterations_done += 1

            if self._shuffle:
                permute = torch.randperm(self._training_size)
                self._training_inputs = self._training_inputs[permute]
                self._training_outputs = self._training_outputs[permute]

            epoch_loss = 0.
            iteration = 0
            for i in range(0, self._training_size, self._minibatch_size):
                i_end = min(i + self._minibatch_size, self._training_size)
                minibatch_inputs = self._training_inputs[i:i_end]
                minibatch_outputs = self._training_outputs[i:i_end]
                optimizer_instance.zero_grad()
                o = self._net(minibatch_inputs)
                loss = self._criterion(o, minibatch_outputs)
                loss.backward()
                optimizer_instance.step()

                # print('iteration: {}, loss: {}'.format(iteration, loss.data[0]))
                epoch_loss += loss.data[0]
                iteration += 1

            epoch_loss /= iteration
            # print('epoch loss: {}'.format(epoch_loss))
            if epoch_loss < self._fit_threshold:
                self._has_finished = True
                return CallResult(None)
        return CallResult(None)

    def _get_params(self, state_dict) -> Params:
        s = {k: v.numpy() for k, v in state_dict.items()}
        return Params(state=s)

    def get_params(self) -> Params:
        return self._get_params(self._net.state_dict())

    def set_params(self, *, params: Params) -> None:
        state = self._net.state_dict()
        new_state = {k: torch.from_numpy(v) for k, v in params['state'].items()}
        state.update(new_state)
        self._net.load_state_dict(state)

    def backward(self, *, gradient_outputs: Gradients[Outputs], fine_tune: bool = False, fine_tune_learning_rate: float = 0.00001,
                 fine_tune_weight_decay: float = 0.00001) -> Tuple[Gradients[Inputs], Scores[Params]]:  # type: ignore

        if self._inputs is None:
            raise Exception('Cannot call backpropagation before forward propagation. Call "produce" before "backprop".')
        else:
            optimizer_instance = optim.SGD(self._net.parameters(), lr=fine_tune_learning_rate, weight_decay=fine_tune_weight_decay)
            if self._inputs.grad is not None:
                self._inputs.grad.data.zero_()
            optimizer_instance.zero_grad()
            self._outputs.backward(gradient=to_variable(gradient_outputs))

            if fine_tune:
                optimizer_instance.step()

            grad_inputs = self._inputs.grad
            # Includes both trainable parameters (which will have derivatives) and state variables such as running averages (which won't have derivatives)
            grad_params_state_dict = {k: v.clone().fill_(0) for k, v in self._net.state_dict().items()}
            # Includes only trainable parameters (which will have derivatives)
            named_parameters = {k: v.grad.data for k, v in self._net.named_parameters()}
            grad_params_state_dict = {k: named_parameters[k] if k in named_parameters else v for k, v in grad_params_state_dict.items()}
            grad_params = self._get_params(grad_params_state_dict)
            return grad_inputs, grad_params  # type: ignore

    def gradient_output(self, *, outputs: Outputs, inputs: Inputs) -> Gradients[Outputs]:
        raise NotImplementedError()

    def gradient_params(self, *, outputs: Outputs, inputs: Inputs) -> Scores[Params]:
        raise NotImplementedError()

    def set_fit_term_temperature(self, *, temperature: float = 0) -> None:
        raise NotImplementedError()
