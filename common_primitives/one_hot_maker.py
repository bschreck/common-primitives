from functools import reduce
from numbers import Number
import os
import time
from typing import Any, Dict, List
from .utils import denumpify
import numpy as np  # type: ignore

from d3m_metadata.container.numpy import ndarray
from d3m_metadata.container.pandas import DataFrame
from d3m_metadata import hyperparams, params, metadata as metadata_module, utils
from primitive_interfaces.unsupervised_learning import UnsupervisedLearnerPrimitiveBase
from primitive_interfaces.base import CallResult


# dict where each value is a list containing an unknown data type
Inputs = DataFrame
# same as input but with each discrete data type converted to a one hot encoding and data vector for each key concatenated
Outputs = ndarray
# each key has a vector such that the encoding for vector[2] is [0 0 1 0 0 0 ...]


class Params(params.Params):
    categories: Dict[Any, List[Any]]


def get_categories(discrete_list):
    values = []
    for value in discrete_list:
        if value not in values:
            values.append(value)
    return values


class Hyperparams(hyperparams.Hyperparams):
    cutoff_for_categorical = hyperparams.Uniform(
        default=0.05,
        lower=0,
        upper=1,
        description='The value used to guess if labels are discrete'
    )


class OneHotMaker(UnsupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    Attempts to detect discrete values in data and convert these to a
    one-hot embedding.
    """

    __author__ = 'Oxford DARPA D3M Team, William Harvey <willh@robots.ox.ac.uk>'
    metadata = metadata_module.PrimitiveMetadata({
         'id': 'eaec420d-46eb-4ddf-a2cd-b8097345ff3e',
         'version': '0.1.0',
         'name': 'One-hot maker',
         'keywords': ['data processing', 'one-hot'],
         'source': {
            'name': 'common-primitives',
            'contact': 'mailto:willh@robots.ox.ac.uk'
         },
         'installation': [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(git_commit=utils.current_git_commit(os.path.dirname(__file__)))
         }],
         'python_path': 'd3m.primitives.common_primitives.OneHotMaker',
         'algorithm_types': ['ENCODE_ONE_HOT'],
         'primitive_family': 'DATA_PREPROCESSING',
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, str] = None) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)
        self._cutoff_for_categorical = self.hyperparams['cutoff_for_categorical']
        self._training_input = {}  # type: ignore

    def set_training_data(self, *, inputs: Inputs) -> None:  # type: ignore
        """ All inputs should have the same keys - the lists for each key are then concatenated """
        inputs = {key: [denumpify(i) for i in inputs[key]] for key in inputs}  # type: ignore
        self._training_input = {}
        for key in inputs:
            self._training_input[key] = reduce(lambda a, b: a+b, [inputs[key]])

    def get_params(self) -> Params:
        return Params(categories=self._categories)

    def set_params(self, *, params: Params) -> None:
        self._categories = params.categories

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        """ Fit based on provided training data.
        """
        start = time.time()
        if self._training_input is None:
            raise ValueError("Missing training data.")

        self._categories = {}
        for key in self._training_input:
            if timeout and time.time() > start + timeout:
                raise TimeoutError

            seen_values = []  # type: List[Any]
            for value in self._training_input[key]:
                if value not in seen_values:
                    seen_values.append(value)
            if len(seen_values) > self._cutoff_for_categorical * len(self._training_input[key]) and isinstance(seen_values[0], Number):    # try to infer whether or not the data is discrete
                self._categories[key] = None
            else:
                self._categories[key] = seen_values
        return CallResult(None)

    def produce(self, *, inputs: Inputs,
                timeout: float = None,
                iterations: int = None) -> CallResult[Outputs]:
        inputs = {key: [denumpify(i) for i in inputs[key]] for key in inputs}  # type: ignore
        return CallResult(np.array(self._produce_one(input=inputs, allow_unseen_categories=True)))

    def _produce_one(self, *, input: dict,
                     allow_unseen_categories) -> ndarray:
        one_hot = {}
        for key in input:
            if self._categories[key] is None:
                one_hot[key] = input[key]
            else:
                # Do some transform using Params.
                one_hot[key] = [[0] * len(self._categories[key]) for _ in input[key]]
                for i in range(len(input[key])):  # iterate through each data point
                    try:
                        one_hot[key][i][self._categories[key].index(input[key][i])] = 1
                    except ValueError:
                        if input[key][i] not in self._categories[key]:
                            if allow_unseen_categories:
                                pass
                            else:
                                raise ValueError("Value {} not encountered during fit".format(input[key][i]))

        # Now concatenate vector for each key:
        result_vectors = [[] for _ in next(iter(one_hot.values()))]  # type: List[List[Any]]
        for key in one_hot:
            for i in range(len(one_hot[key])):
                try:
                    result_vectors[i].extend(one_hot[key][i])
                except TypeError:  # if one_hot[key][i] is not a list
                    result_vectors[i].append(one_hot[key][i])
        return np.array(result_vectors)
