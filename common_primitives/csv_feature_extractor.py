import pandas as pd  # type: ignore
import os

from typing import Dict
from d3m_metadata.container.list import List
from d3m_metadata.container.pandas import DataFrame
from d3m_metadata import hyperparams, metadata as metadata_module, utils
from primitive_interfaces.transformer import TransformerPrimitiveBase
from primitive_interfaces.base import CallResult, SingletonOutputMixin

Void = type(None)

Inputs = List[str]
Outputs = DataFrame
Params = Void  # type: ignore


class Hyperparams(hyperparams.Hyperparams):
    """
    No hyper-parameters for this primitive.
    """
    pass


class CSVFeatureExtractor(SingletonOutputMixin[Inputs, Outputs, Params, Hyperparams],
                          TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    Primitive which takes a path to a CSV file of named columns and returns
    a Pandas DataFrame.
    """
    __author__ = 'Oxford DARPA D3M Team, William Harvey <willh@robots.ox.ac.uk>'
    metadata = metadata_module.PrimitiveMetadata({
         'id': '8574ab12-1c9e-49c9-ae92-607c9575afb4',
         'version': '0.1.0',
         'name': 'CSV feature extractor',
         'keywords': ['data processing', 'csv'],
         'source': {
            'name': 'common-primitives',
            'contact' : 'mailto:willh@robots.ox.ac.uk'
         },
         'installation': [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(git_commit=utils.current_git_commit(os.path.dirname(__file__)))
         }],
         'python_path': 'd3m.primitives.common_primitives.CSVFeatureExtractor',
         'algorithm_types': ['NUMERICAL_METHOD'],
         'primitive_family': 'DATA_PREPROCESSING',
    })

    def __init__(self, *, hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, str] = None) -> None:
        super().__init__(hyperparams=hyperparams,
                         random_seed=random_seed,
                         docker_containers=docker_containers)

    def produce(self, *,
                inputs: List[str],
                timeout: float = None,
                iterations: int = None) -> CallResult[Outputs]:
        if len(inputs) != 1:
            raise ValueError('Expecting a list of one string with the file name.')
        df = pd.read_csv(*inputs)
        return CallResult(df.reindex(sorted(df.columns), axis=1))
