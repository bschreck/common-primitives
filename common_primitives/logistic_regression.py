from typing import Sequence, Dict

import os
import tempfile

try:
    open('~/.theanorc', 'a+')
except FileNotFoundError:
    compile_dir = tempfile.TemporaryDirectory()
    os.environ['THEANO_FLAGS'] = 'base_compiledir=' + compile_dir.name

import numpy as np  # type: ignore
from pymc3 import Model, Normal, Bernoulli, NUTS  # type: ignore
from pymc3 import invlogit, sample, sample_ppc  # type: ignore
import pymc3 as pm  # type: ignore
from pymc3.backends.base import MultiTrace  # type: ignore

from d3m_metadata.container.numpy import ndarray
from d3m_metadata import hyperparams, params, metadata as metadata_module, utils
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from primitive_interfaces.base import ProbabilisticCompositionalityMixin
from primitive_interfaces.base import SamplingCompositionalityMixin
from primitive_interfaces.base import CallResult
import theano  # type: ignore


# n_inputs x n_dimensions array of floats
Inputs = ndarray
# 1D int array of length n_inputs
Outputs = ndarray

Input = ndarray
Output = int


class Params(params.Params):
    weights: MultiTrace


class Hyperparams(hyperparams.Hyperparams):
    burnin = hyperparams.Hyperparameter[int](
        default=1000,
        description='The number of samples to take before storing them'
    )


class BayesianLogisticRegression(ProbabilisticCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                                 SamplingCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                                 # Base class should be after all mixins.
                                 SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    Example of a primitive wrapping logistic regression using PyMC3 and its
    Theano backend.
    """

    __author__ = "Oxford DARPA D3M Team"
    metadata = metadata_module.PrimitiveMetadata({
        "id": "87dfa2c3-d0de-48aa-b3e5-814496ae211e",
        "version": "0.1.0",
        "source": {'name': 'common-primitives'},
        'installation': [{'type': metadata_module.PrimitiveInstallationType.PIP,
                          'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(
                              git_commit=utils.current_git_commit(os.path.dirname(__file__)),
                          )}],
        "name": "common_primitives.BayesianLogisticRegression",
        "python_path": "d3m.primitives.common_primitives.BayesianLogisticRegression",
        "algorithm_types": ["MARKOV_CHAIN_MONTE_CARLO"],
        "primitive_family": "CLASSIFICATION",
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, str] = None) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)
        self._burnin = self.hyperparams['burnin']
        self._seed = -1
        self._fitted = False
        self._training_inputs = None  # type: ndarray
        self._training_outputs = None  # type: ndarray
        self._trace = None  # type: MultiTrace
        self._model = None  # type: Model

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self._training_inputs = inputs
        self._training_outputs = outputs
        self._fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        """
        Sample a Bayesian Logistic Regression model using NUTS to find
        some reasonably weights
        """
        if self._fitted:
            return CallResult(None)

        if self._training_inputs is None or self._training_outputs is None:
            raise ValueError("Missing training data.")

        # training data needs to be a Theano shared variable for
        # the later produce code to work
        _, n_features = self._training_inputs.shape
        self._training_inputs = theano.shared(self._training_inputs)
        self._training_outputs = theano.shared(self._training_outputs)

        # As the model depends on number of features it has to be here
        # and not in __init__
        with Model() as model:
            weights = Normal('weights', 0, 1, shape=n_features)
            p = invlogit(pm.math.dot(self._training_inputs, weights))
            Bernoulli('y', p, observed=self._training_outputs)
            step = NUTS()
            # TODO: Implement this so that one can call iteratively with multiple iterations.
            trace = sample(1000, step, random_seed=self._seed,
                           tune=self._burnin, progressbar=False)

        self._model = model
        self._trace = trace
        self._fitted = True
        return CallResult(None)

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        # Set shared variables to test data, outputs just need to be
        # the correct shape
        self._training_inputs.set_value(inputs)
        self._training_outputs.set_value(np.random.binomial(1, 0.5, inputs.shape[0]))

        with self._model:
            post_pred = sample_ppc(self._trace, samples=50, progressbar=False)
        return CallResult(np.random.binomial(1, post_pred['y'].mean(axis=0)).astype(int))

    def sample(self, *, inputs: Inputs, num_samples: int = 1, timeout: float = None, iterations: int = None) -> CallResult[Sequence[Outputs]]:
        # Set shared variables to test data, outputs just need to be
        # the correct shape
        self._training_inputs.set_value(inputs)
        self._training_outputs.set_value(np.random.binomial(1, 0.5, inputs.shape[0]))

        with self._model:
            post_pred = sample_ppc(self._trace,
                                   samples=num_samples,
                                   progressbar=False)
        return CallResult(post_pred['y'].astype(int))

    def _log_likelihood(self, *, output: Output) -> float:
        "Provides a likelihood of one input/output pair given the weights"
        logp = self._model.logp
        weights = self._trace["weights"]
        return float(np.array([logp(dict(y=output,
                                         weights=w)) for w in weights]).mean())

    def log_likelihoods(self, *,
                        outputs: Outputs,
                        inputs: Inputs,
                        timeout: float = None,
                        iterations: int = None) -> CallResult[Sequence[float]]:
        "Provides a likelihood of the data given the weights"
        return CallResult([sum(self._log_likelihood(output=output) for output in outputs)])

    def get_params(self) -> Params:
        return Params(weights=self.trace)

    def set_params(self, *, params: Params) -> None:
        self._trace = params.weights

    def set_random_seed(self, *, seed: int) -> None:
        self._seed = seed
