import os
from typing import Dict, Union
from d3m_metadata.container.numpy import ndarray

from sklearn.cluster import KMeans as KMeans_  # type: ignore

from primitive_interfaces.unsupervised_learning import UnsupervisedLearnerPrimitiveBase
from d3m_metadata import hyperparams, params, metadata as metadata_module, utils
from primitive_interfaces.base import CallResult

# These are just regular Python variables so that we can easily change all types
# at once in the future, if needed. Otherwise, one could simply inline all these.
Inputs = ndarray
Outputs = ndarray

# A named tuple for parameters.
# Specifying types for all parameters is important so that one can do end-to-end
# training from outside. For example, some parameters might have gradients so we
# can use those to optimize them end-to-end.


class Params(params.Params):
    cluster_centers: ndarray  # Coordinates of cluster centers.


class Hyperparams(hyperparams.Hyperparams):
    n_clusters = hyperparams.Hyperparameter[int](
                            default=8,
                            description='Number of clusters to use by default for :meth:`k_means` queries. '
                            )
    init = hyperparams.Hyperparameter[Union[str, ndarray]](
                            default='k-means++',
                            description='no idea'
                            )
    n_init = hyperparams.Hyperparameter[int](
                            default=10,
                            description='no idea'
                            )
    max_iter = hyperparams.Hyperparameter[int](
                            default=300,
                            description='self explanatory'
                            )


class KMeans(UnsupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    Example of a primitive wrapping an existing sklearn clustering algorithm.
    """

    __author__ = 'Oxford DARPA D3M Team'
    metadata = metadata_module.PrimitiveMetadata({
        'id': '30ac35ef-b4af-4396-b048-a75afae68068',
        'version': '0.1.0',
        'name': 'sklearn.neighbors.classification.KNeighborsClassifier',
        'source': {'name': 'common-primitives'},
        'installation': [{'type': 'PIP',
                          'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(
                              git_commit=utils.current_git_commit(os.path.dirname(__file__)),
                          )}],
        'python_path': 'd3m.primitives.common_primitives.KMeans',
        'algorithm_types': ['K_MEANS_CLUSTERING'],
        'primitive_family': 'CLUSTERING'
    })

    # It is important that all hyper-parameters (parameters which do not change during
    # a life-cycle of a primitive) are explicitly listed and typed in the constructor.
    # This allows one to do hyper-parameter tuning and explores the space of
    # hyper-parameters.
    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, str] = None) -> None:
        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)

        self._clf = KMeans_(n_clusters=hyperparams['n_clusters'],
                            init=hyperparams['init'],
                            n_init=hyperparams['n_init'],
                            max_iter=hyperparams['max_iter'], random_state=random_seed)
        self._training_inputs = None  # type: Inputs
        self._fitted = False

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        return CallResult(self._clf.predict(inputs))

    def set_training_data(self, *, inputs: Inputs) -> None:  # type: ignore
        self._training_inputs = inputs
        self._fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        # If already fitted with current training data, this call is a noop.
        if self._fitted:
            return CallResult(None)

        if self._training_inputs is None:
            raise ValueError("Missing training data.")

        self._clf.fit(self._training_inputs)
        self._fitted = True

        return CallResult(None)

    def get_params(self) -> Params:
        return Params(
                    cluster_centers=self._clf.cluster_centers)

    def set_params(self, *, params: Params) -> None:
        self._clf.cluster_centers = params.cluster_centers
