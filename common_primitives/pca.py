from typing import Dict

import os
import torch  # type: ignore
import numpy as np  # type: ignore

from d3m_metadata.container import ndarray
from primitive_interfaces.base import PrimitiveBase, CallResult
from d3m_metadata import hyperparams, params, metadata as metadata_module, utils

# These are just regular Python variables so that we can easily change all types
# at once in the future, if needed. Otherwise, one could simply inline all these.
Inputs = ndarray
Outputs = ndarray

# A named tuple for parameters.
# Specifying types for all parameters is important so that one can do end-to-end
# training from outside. For example, some parameters might have gradients so we
# can use those to optimize them end-to-end.


class Params(params.Params):
    transformation: ndarray
    n_components: int


class Hyperparams(hyperparams.Hyperparams):
    max_components = hyperparams.Hyperparameter[int](
        default=0,
        description="?"
    )
    proportion_variance = hyperparams.Hyperparameter[float](
        default=1.0,
        description="?"
    )


def remove_mean(data):
    """
    input: NxD torch array
    output: D-length mean vector, NxD torch array

    takes a torch tensor, calculates the mean of each
    column and subtracts it

    returns (mean, zero_mean_data)
    """

    N, D = data.size()
    mean = torch.zeros([D]).type(torch.DoubleTensor)
    for row in data:
        mean += row.view(D)/N
    zero_mean_data = data - mean.view(1, D).expand(N, D)
    return mean, zero_mean_data


class PCA(PrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    Example of a dimensionality reduction primitive.
    """

    __author__ = "William Harvey <willh@robots.ox.ac.uk>"
    metadata = metadata_module.PrimitiveMetadata({
        "id": "1ca1285d-af54-47d3-abfe-42b22924bb3f'",
        "version": "0.1.0",
        "name": "PCA",
        "team": "Oxford DARPA D3M \"Hasty\" team",
        "common_name": "Principal Component Analysis",
        "source": {"name": "common-primitives"},
        "installation": [{'type': metadata_module.PrimitiveInstallationType.PIP,
                          'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(
                               git_commit=utils.current_git_commit(os.path.dirname(__file__)),
                           ),
         }],
        "python_path": "d3m.primitives.common_primitives.PCA",
        "algorithm_types": ['PRINCIPAL_COMPONENT_ANALYSIS'],
        "primitive_family": "FEATURE_EXTRACTION",
        "task_type": ["Feature Extraction"],
        "compute_resources": {
            "sample_size": [],
            "sample_unit": [],
            "disk_per_node": [],
            "expected_running_time": [],
            "gpus_per_node": [],
            "cores_per_node": [],
            "mem_per_gpu": [],
            "mem_per_node": [],
            "num_nodes": [],
        },
        "learning_type": ["Unsupervised Learning"],
        "handles_regression": False,
        "handles_classification": False,
        "handles_multiclass": False,
        "handles_multilabel": False
    })

    # It is important that all hyper-parameters (parameters which do not change during
    # a life-cycle of a primitive) are explicitly listed and typed in the constructor.
    # This allows one to do hyper-parameter tuning and explores the space of
    # hyper-parameters.
    def __init__(self, *, hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, str] = None) -> None:
        """
        If both max_components and proportion_variance are set, the one which gives the
        fewest principal components is used
        """
        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)

        self._max_components = hyperparams['max_components']
        self._n_components = None  # type: Optional[int]
        self._proportion_variance = hyperparams['proportion_variance']
        self._training_inputs = None  # type: Optional[torch.Variable]
        self._fitted = False
        self._transformation = None  # type: Optional[torch.Variable]
        self._mean = None

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        inputs = torch.from_numpy(np.array(inputs)).type(torch.DoubleTensor)
        return CallResult(np.array([torch.mv(self._transformation, row - self._mean).numpy() for row in inputs]))

    def set_training_data(self, *, inputs: Inputs) -> None:
        self._training_inputs = torch.from_numpy(inputs).type(torch.DoubleTensor)
        self._fitted = False
        self._n_components = None

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        # If already fitted with current training data, this call is a noop.
        if self._fitted:
            return CallResult(None)

        if self._training_inputs is None:
            raise ValueError("Missing training data.")

        # Get eigenvectors of covariance
        self._mean, zero_mean_data = remove_mean(self._training_inputs)
        cov = torch.from_numpy(np.cov(self._training_inputs.numpy().T))
        cov = cov.type(torch.FloatTensor)
        e, V = torch.eig(cov, True)
        # Choose which/how many eigenvectors to use
        total_variance = sum(np.linalg.norm(e.numpy()[i, :])
                             for i in range(e.size()[0]))
        indices = []
        self._n_components = 0
        recovered_variance = 0
        while recovered_variance < self._proportion_variance * total_variance \
                and (self._max_components == 0 or self._n_components < self._max_components):
            best_index = max(range(e.size()[0]), key=lambda x: np.linalg.norm(e.numpy()[x, :]))
            indices.append(best_index)
            recovered_variance += np.linalg.norm(e.numpy()[best_index, :])
            e[best_index, :] = torch.zeros(2)
            self._n_components += 1

        # Construc transformation matrix with eigenvectors
        self._transformation = torch.zeros([self._n_components, self._training_inputs.size()[1]]).type(torch.DoubleTensor)
        for n, indice in enumerate(indices):
            self._transformation[n, :] = V[:, indice]

        self._fitted = True
        CallResult(None)

    def get_params(self) -> Params:
        return Params(transformation=ndarray(self._transformation.numpy()),
                      n_components=self._n_components)

    def set_params(self, *, params: Params) -> None:
        self._transformation = torch.from_numpy(params.transformation).type(torch.DoubleTensor)
