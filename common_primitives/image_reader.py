
from typing import Dict
from d3m_metadata.container.list import List
from d3m_metadata.container.numpy import ndarray
from d3m_metadata import hyperparams, metadata as metadata_module, utils
from primitive_interfaces.transformer import TransformerPrimitiveBase
from primitive_interfaces.base import CallResult, SingletonOutputMixin
import os
import numpy as np
import scipy.ndimage

Inputs = List[str]
Outputs = ndarray


class Hyperparams(hyperparams.Hyperparams):
    """
    No hyper-parameters for this primitive.
    """
    pass


class ImageReader(SingletonOutputMixin[Inputs, Outputs, None, Hyperparams],
                  TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    Primitive which takes a list of file names to image files and returns a single Numpy array
    of shape NxCxHxW where N is the number of images, C is the number of channels in an image (e.g., C = 1 for grayscale, C = 3 for RGB),
    H is the height, and W is the width. All images are expected to be of the same shape and channel characteristics.
    """
    __author__ = 'Oxford DARPA D3M Team, Atilim Gunes Baydin <robots.ox.ac.uk>'
    metadata = metadata_module.PrimitiveMetadata({
         'id': '006251ce-4489-411b-9979-07782edb9085',
         'version': '0.1.0',
         'name': 'Image reader',
         'keywords': ['image', 'jpg', 'png', 'tiff'],
         'source': {
            'name': 'common-primitives',
            'contact' : 'mailto:gunes@robots.ox.ac.uk'
         },
         'installation': [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(git_commit=utils.current_git_commit(os.path.dirname(__file__)))
         }],
         'python_path': 'd3m.primitives.common_primitives.ImageReader',
         'algorithm_types': ['NUMERICAL_METHOD'],
         'primitive_family': 'DATA_PREPROCESSING',
    })

    def __init__(self, *, hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers : Dict[str, str] = None) -> None:
        super().__init__(hyperparams=hyperparams,
                         random_seed=random_seed,
                         docker_containers=docker_containers)

    def produce(self, *,
                inputs: List[str],
                timeout: float = None,
                iterations: int = None) -> CallResult[Outputs]:
        if len(inputs) < 1:
            raise ValueError('Expecting a list of more than one string with file names.')
        images = []
        image_shape = None
        for file_name in inputs:
            i = scipy.ndimage.imread(file_name)
            if image_shape is None:
                image_shape = i.shape
            else:
                if image_shape != i.shape:
                    raise ValueError('Expecting all images to be of the same shape.')
            if i.ndim == 2:
                i = np.expand_dims(i, axis=0)
            elif i.ndim == 3:
                i = i.transpose((2,0,1))
            else:
                raise ValueError('Unsupported image.')
            images.append(i)
        ret = np.stack(images)
        return CallResult(ret)
