import unittest

import numpy as np

from common_primitives.diagonal_mvn import diagonal_mvn
from common_primitives.linear_regression import linear_regression


class TestLinearRegression(unittest.TestCase):
    def test_fit(self):
        # Calculate fit using the primitives
        regularizer = diagonal_mvn.DiagonalMVN()
        regularizer.set_params(params=diagonal_mvn.Params(mean=np.array([0.0, 0.0, 0.0]),
                                                          covariance=np.identity(3)*0.1))
        linreg = linear_regression.LinearRegression()
        train_y = np.array([-2, -1, 0, 1, 2, 3])
        train_x = np.array([[10, 9, 8], [3.5, 3, 0], [7, 2, 1], [8, 1, 3], [9, 0, 13], [9, 0, 13]])
        linreg.set_training_data(outputs=train_y, inputs=train_x)

        linreg.fit(iterations=300, weights_prior=regularizer)
        w_primitive = linreg.get_params().weights

        # Calculate fit analytically assuming noise variance is correct
        train_x_with_ones = np.insert(train_x, 0, 1, axis=1)
        inv_covar = np.zeros([4, 4])
        inv_covar[1:4, 1:4] = np.linalg.inv(regularizer.get_params().covariance)
        w_analytic = np.dot(np.dot(np.linalg.inv(np.dot(np.transpose(train_x_with_ones), train_x_with_ones)
                                   + inv_covar * linreg.get_params().noise_variance), np.transpose(train_x_with_ones)), train_y)

        self.assertTrue(linreg.get_call_metadata().iterations_done == 300)
        self.assertTrue(np.linalg.norm(w_primitive-w_analytic[1:4]) < 0.02)

    def test_noise_prior(self):
        """
        nothing yet exists to provide a prior for a float
        """

    def test_batches(self):
        linreg = linear_regression.LinearRegression(alpha=5e-3,
                                                    beta=0)
        linreg.set_random_seed(seed=0)
        # train_x = np.array([[9, 8], [3, 2], [7, 2], [5, 4], [5, 5], [9, 3]])
        train_x = np.array([[3, 4], [-3, -2], [0, -2], [2, -3], [1, 1], [-3, 2]])
        weights = np.array([1, 0.4])
        train_y = np.dot(train_x, weights) + 0.000001*np.random.randn(len(train_x))
        linreg.set_training_data(inputs=train_x, outputs=train_y)
        linreg.fit(iterations=300, batch_size=3)
        pred_weights = linreg.get_params().weights
        print(pred_weights)
        self.assertTrue(
            np.linalg.norm(weights-pred_weights)
            < 0.1)

    def test_gradients(self):
        linreg = linear_regression.LinearRegression()
        linreg.set_params(params=linear_regression.Params(weights=np.array([1, 1]), offset=0, noise_variance=2))
        grad = linreg.gradient_output(outputs=np.array([3]), inputs=np.array([[1, 1]]))

        # https://www.wolframalpha.com/input/?i=d%2Fdy(-+(y-2)%5E2+)%2F(2*2)+at+y+%3D+3
        expected_grad = -0.5

        self.assertEqual(grad, expected_grad)

        # next test grad_params similarly...

    def test_sample(self):
        linreg = linear_regression.LinearRegression()
        linreg.set_params(params=linear_regression.Params(weights=np.array([1, 1]), offset=0, noise_variance=0.0001))

        # Check second sample is within 50 SDs of the expected value.
        self.assertTrue(3.5 < linreg.sample(inputs=np.array([[3, 1]]), num_samples=2)[0][0] < 4.5)

        # check second sample is within 50 SDs of the expected value
        assert 3.5 < linreg.sample(inputs=np.array([[3, 1]]), num_samples=2)[1][0] < 4.5

    def test_produce(self):
        linreg = linear_regression.LinearRegression()
        linreg.set_params(params=linear_regression.Params(weights=np.array([1, 1]), offset=0, noise_variance=0.0001))
        p = linreg.produce(inputs=np.array([[3, 1]]))[0]

        assert p == 4

    def test_log_likelihood(self):
        linreg = linear_regression.LinearRegression()
        linreg.set_params(params=linear_regression.Params(weights=np.array([1, 1]), offset=0, noise_variance=0.0001))
        assert linreg.log_likelihood(outputs=np.array([2.1, 3.1]),
                                     inputs=np.array([[1, 1], [2, 1]])) >\
            linreg.log_likelihood(outputs=[2.2, 3],
                                  inputs=[np.array([1, 1]), np.array([2, 1])])


if __name__ == '__main__':
    unittest.main()
