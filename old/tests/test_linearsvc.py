import unittest

from common_primitives.linearSVC import linearSVC

from sklearn.model_selection import train_test_split
from sklearn import datasets

from random import randint
import numpy as np


class TestLinearSVC(unittest.TestCase):
    def test_fit(self):
        lsvc = linearSVC.LinearSVC()

        iris = datasets.load_iris()
        X_train, X_test, y_train, y_test = train_test_split(iris.data, iris.target, test_size=0.25, random_state=42)
        
        lsvc.set_training_data(inputs=X_train, outputs=y_train)
        lsvc.fit()

        sample_1 = X_test[randint(0,len(y_test)-1)]
        sample_2 = X_test[randint(0,len(y_test)-1)]
        assert np.array_equal(lsvc.produce(inputs=[sample_1, sample_2]),
                              lsvc.produce(inputs=np.vstack((sample_1, sample_2))))


if __name__ == '__main__':
    unittest.main()
