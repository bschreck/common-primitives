import unittest

import numpy as np
from numpy import testing

from common_primitives.diagonal_mvn import diagonal_mvn


class TestDiagonalMVN(unittest.TestCase):
    def test_fit(self):
        # Make up some data.
        observations = np.array([[0, 0], [1, 2]])
        true_mean = np.array([0.5, 1])
        true_cov = np.diag([0.25, 1])  # Since it must be diagonal.

        # Calculate fit using the primitives
        a = diagonal_mvn.DiagonalMVN()
        a.set_training_data(outputs=observations)
        a.fit(iterations=200)

        self.assertTrue(a.get_call_metadata().iterations_done == 200)
        self.assertTrue(np.linalg.norm(true_mean - a.get_params().mean) < 0.02)
        self.assertTrue(np.linalg.norm(true_cov - a.get_params().covariance) < 0.02)

    def test_gradients_dont_add(self):
        a = diagonal_mvn.DiagonalMVN()
        a.set_params(params=diagonal_mvn.Params(mean=np.array([0.5, 1, 3]), covariance=np.diag([0.25, 1, 0.1])))

        testing.assert_array_equal(a.gradient_output(outputs=np.array([[0, 0, 2], [1, 2, 0]])),
                                   a.gradient_output(outputs=np.array([[0, 0, 2], [1, 2, 0]])))
        testing.assert_array_equal(a.gradient_params(outputs=np.array([[0, 0, 2], [1, 2, 0]])).mean,
                                   a.gradient_params(outputs=np.array([[0, 0, 2], [1, 2, 0]])).mean)
        testing.assert_array_equal(a.gradient_params(outputs=np.array([[0, 0, 2], [1, 2, 0]])).covariance,
                                   a.gradient_params(outputs=np.array([[0, 0, 2], [1, 2, 0]])).covariance)

    def test_gradients(self):
        a = diagonal_mvn.DiagonalMVN()
        a.set_params(params=diagonal_mvn.Params(mean=np.array([0.5, 1]), covariance=np.diag([0.25, 1])))

        testing.assert_array_equal(a.gradient_output(outputs=np.array([[0, 0], [1, 2]])),
                                   a.gradient_output(outputs=np.array([[0, 0]])) +
                                   a.gradient_output(outputs=np.array([[1, 2]])))

        testing.assert_array_equal(a.gradient_output(outputs=np.array([[1, 1]])),
                                   np.array([-2, 0]))

    def test_log_likelihood(self):
        a = diagonal_mvn.DiagonalMVN()
        a.set_params(params=diagonal_mvn.Params(mean=np.array([0.5, 1]), covariance=np.diag([0.25, 1])))
        assert a.log_likelihood(outputs=[np.array([1, 1]), np.array([2, 1])]) <\
            a.log_likelihood(outputs=[np.array([1, 1]), np.array([1, 2])])

    def test_sample(self):
        a = diagonal_mvn.DiagonalMVN()
        a.set_params(params=diagonal_mvn.Params(mean=np.array([0.5, 1]), covariance=np.diag([0.25, 1])))
        s = a.sample(inputs=[None, None, None], num_samples=2)
        assert(s.shape == (2, 3, 2))

    def test_produce(self):
        a = diagonal_mvn.DiagonalMVN()
        a.set_params(params=diagonal_mvn.Params(mean=np.array([0.5, 1]), covariance=np.diag([0.25, 1])))
        P = a.produce(inputs=[None, None])
        p1, p2 = P[0], P[1]

        testing.assert_array_equal(p1, p2)
        testing.assert_array_equal(a.get_params().mean, p1)


if __name__ == '__main__':
    unittest.main()
