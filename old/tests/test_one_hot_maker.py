import unittest

import os
import numpy as np

from common_primitives.one_hot_maker import one_hot_maker
from common_primitives.csv_feature_extractor import csv_feature_extractor


class Test(unittest.TestCase):
    def test_one_hotting(self):
        TEST_FILE = os.path.dirname(os.path.realpath(__file__)) + "/test_data_csv/test.csv"
        extractor = csv_feature_extractor.CSVFeatureExtractor()
        datas = extractor.produce(inputs=[TEST_FILE])

        one_hotter = one_hot_maker.OneHotMaker()

        one_hotter.set_training_data(inputs=[{"value": [0.0, 1.0, 2.0, 3.0, 4.0, 5.0],
                                              "number": [0, 1, 2, 0, 1, 2, 0, 1, 3, 3],
                                              "word": ["one", "two", "three"]}])
        one_hotter.fit()

        one_hots = one_hotter.produce(inputs=datas)

        assert type(one_hots[0]) == np.ndarray


if __name__ == '__main__':
    unittest.main()
