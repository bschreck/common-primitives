import numpy as np
from common_primitives.csv_feature_extractor.csv_feature_extractor import CSVFeatureExtractor
from common_primitives.one_hot_maker import one_hot_maker
from common_primitives.mlp.mlp import MLP

csv_extractor = CSVFeatureExtractor()
x_file = "data/radon_train.csv"
y_file = "data/trainTargets.csv"
extracted_x = csv_extractor.produce(inputs=[x_file])[0]
extracted_x.pop("d3mIndex")
extracted_x.pop("idnum")
extracted_y = csv_extractor.produce(inputs=[y_file])[0]
extracted_y.pop("d3mIndex")

onehotter = one_hot_maker.OneHotMaker()
onehotter.set_training_data(inputs=[extracted_x])
onehotter.fit()
training_inputs = onehotter.produce(inputs=[extracted_x])[0]
training_outputs = np.array(next(iter(extracted_y.values())))

mlp = MLP(depth=4, width=64, activation='tanh', linear_output=True)
mlp.set_training_data(inputs=training_inputs, outputs=training_outputs)
mlp.fit(iterations=100)

output = mlp.produce(inputs=training_inputs).reshape(-1)
mse = np.sum((output-training_outputs)**2) / len(training_outputs)
print('MSE: ', mse)
