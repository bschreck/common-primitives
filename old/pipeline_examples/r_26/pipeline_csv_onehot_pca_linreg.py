import numpy as np
from functools import reduce

from common_primitives.diagonal_mvn import diagonal_mvn
from common_primitives.linear_regression import linear_regression

from common_primitives.csv_feature_extractor import csv_feature_extractor
from common_primitives.one_hot_maker import one_hot_maker
from common_primitives.linear_regression import linear_regression
from common_primitives.diagonal_mvn import diagonal_mvn
from common_primitives.pca import pca

"""
  Input
    |
CSVFeatureExtractor
    |
OneHotMaker
    |
   PCA
    |
LinearRegression <- DiagonalMVN
    |
  Output
"""


class RegularisedLinRegPipeline():
    def __init__(self):
        """ Create an instance of each primitive """
        self.csv_extractor = csv_feature_extractor.CSVFeatureExtractor()
        self.vectorizer = one_hot_maker.OneHotMaker()
        self.linreg = linear_regression.LinearRegression(alpha=0.001,
                                                         beta=1e-9)
        self.regularizer = diagonal_mvn.DiagonalMVN()
        self.pca = pca.PCA(proportion_variance=0.9)

    def train(self, x_file, y_file, iterations):
        """ Train the pipeline
            For each primitive, first train the primitive on the data and then
            produce output from it. Train each primitive on the previous one's
            output.
        """
        extracted_x = self.csv_extractor.produce(inputs=[x_file])[0]
        extracted_y = self.csv_extractor.produce(inputs=[y_file])[0]
        extracted_x.pop("d3mIndex")
        extracted_x.pop("idnum")
        extracted_y.pop("d3mIndex")

        self.vectorizer.set_training_data(inputs=[extracted_x])
        self.vectorizer.fit()
        embedded_x = self.vectorizer.produce(inputs=[extracted_x])[0]
        embedded_y = np.array(next(iter(extracted_y.values())))

        self.pca.set_training_data(inputs=embedded_x)
        self.pca.fit()
        prin_comp_x = self.pca.produce(inputs=embedded_x)

        n_components = prin_comp_x.shape[1]
        self.regularizer.set_params(params=diagonal_mvn.Params(mean=np.zeros(n_components), covariance=np.identity(n_components)))
        self.linreg.set_training_data(inputs=prin_comp_x, outputs=embedded_y)

        self.linreg.fit(iterations=iterations, weights_prior=self.regularizer)

    def make_predictions(self, x_file):
        extracted_x = self.csv_extractor.produce(inputs=[x_file])[0]
        extracted_x.pop("d3mIndex")
        extracted_x.pop("idnum")

        embedded_x = self.vectorizer.produce(inputs=[extracted_x])[0]

        prin_comp_x = self.pca.produce(inputs=embedded_x)

        return np.array(self.linreg.produce(inputs=prin_comp_x))

    def RMSE(self, test_x_file, test_y_file):
        predictions = self.make_predictions(test_x_file)

        targets = self.csv_extractor.produce(inputs=[test_y_file])[0]
        targets.pop("d3mIndex")
        targets = np.array(next(iter(targets.values())))

        # Return Mean Squared Error
        return np.sqrt(np.mean((predictions-targets)**2))


# Get data
training_x_file = "data/radon_train.csv"
training_y_file = "data/trainTargets.csv"

pipeline = RegularisedLinRegPipeline()
pipeline.train(training_x_file, training_y_file, iterations=10)    # Ideally run for > 100 but takes a while

print("RMSE:", pipeline.RMSE(training_x_file, training_y_file))
# print(pipeline.linreg.get_params())
