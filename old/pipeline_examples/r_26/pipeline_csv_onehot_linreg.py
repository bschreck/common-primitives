import numpy as np

from common_primitives.csv_feature_extractor import csv_feature_extractor
from common_primitives.one_hot_maker import one_hot_maker
from common_primitives.linear_regression import linear_regression
from common_primitives.diagonal_mvn import diagonal_mvn

"""
  Input
    |
CSVFeatureExtractor
    |
OneHotMaker
    |
LinearRegression <- DiagonalMVN
    |
  Output
"""


class RegularisedLinRegPipeline():
    def __init__(self):
        """ Create an instance of each primitive """
        self.csv_extractor = csv_feature_extractor.CSVFeatureExtractor()
        self.vectorizer = one_hot_maker.OneHotMaker()
        self.linreg = linear_regression.LinearRegression(alpha=0.001,
                                                         beta=1e-10)
        self.regularizer = diagonal_mvn.DiagonalMVN()

    def train(self, x_file, y_file, iterations):
        """ Train the pipeline
            For each primitive, first train the primitive on the data and then
            produce output from it. Train each primitive on the previous one's
            output.
        """
        extracted_x = self.csv_extractor.produce(inputs=[x_file])[0]
        extracted_y = self.csv_extractor.produce(inputs=[y_file])[0]
        extracted_x.pop("d3mIndex")
        extracted_x.pop("idnum")
        extracted_y.pop("d3mIndex")

        self.vectorizer.set_training_data(inputs=[extracted_x])
        self.vectorizer.fit()
        embedded_x = self.vectorizer.produce(inputs=[extracted_x])[0]
        embedded_y = np.array(next(iter(extracted_y.values())))

        n_rows, n_cols = embedded_x.shape
        self.regularizer.set_params(params=diagonal_mvn.Params(mean=np.zeros(n_rows), covariance=np.identity(n_rows)))
        self.linreg.set_training_data(inputs=embedded_x, outputs=embedded_y)

        for i in range(iterations):
            self.linreg.fit(iterations=1, weights_prior=self.regularizer)
            print('Iteration: {}/{}'.format(i, iterations))

    def make_predictions(self, x_file):
        extracted_x = self.csv_extractor.produce(inputs=[x_file])[0]
        extracted_x.pop("d3mIndex")
        extracted_x.pop("idnum")

        embedded_x = self.vectorizer.produce(inputs=[extracted_x])[0]

        return np.array(self.linreg.produce(inputs=embedded_x))

    def MSE(self, test_x_file, test_y_file):
        predictions = self.make_predictions(test_x_file)

        targets = self.csv_extractor.produce(inputs=[test_y_file])[0]
        targets.pop("d3mIndex")
        targets = np.array(next(iter(targets.values())))

        # Return Mean Squared Error
        return np.sqrt(np.mean((predictions-targets)**2))


# Get data
training_x_file = "data/radon_train.csv"
training_y_file = "data/trainTargets.csv"

pipeline = RegularisedLinRegPipeline()
pipeline.train(training_x_file, training_y_file, 10)    # Ideally run for > 100 but takes a while

print("MSE:", pipeline.MSE(training_x_file, training_y_file))
