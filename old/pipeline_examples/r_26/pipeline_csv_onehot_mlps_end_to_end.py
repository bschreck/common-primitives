import os
import sys
import numpy as np
from common_primitives.csv_feature_extractor.csv_feature_extractor import CSVFeatureExtractor
from common_primitives.one_hot_maker import one_hot_maker
from common_primitives.mlp.mlp import MLP
from common_primitives.loss.loss import Loss

# Primitive for reading the CSV dataset
csv_extractor = CSVFeatureExtractor()
x_file = os.path.dirname(os.path.realpath(__file__)) + "/data/radon_train.csv"
y_file = os.path.dirname(os.path.realpath(__file__)) + "/data/trainTargets.csv"
extracted_x = csv_extractor.produce(inputs=[x_file])[0]
extracted_x.pop("d3mIndex")
extracted_x.pop("idnum")
extracted_y = csv_extractor.produce(inputs=[y_file])[0]
extracted_y.pop("d3mIndex")

# Primitive for vectorizing the CSV data
onehotter = one_hot_maker.OneHotMaker(cutoff_for_categorical=0.05)
onehotter.set_training_data(inputs=[extracted_x])
onehotter.fit()
training_inputs = onehotter.produce(inputs=[extracted_x])[0]
training_outputs = np.array(next(iter(extracted_y.values())))
training_size = training_inputs.shape[0]

# Instantiate three primitives deriving from GradientCompositionalityMixin, for end-to-end training
mlp1 = MLP(depth=4, width=64, activation='tanh')
mlp1.set_training_data(inputs=training_inputs, outputs=np.zeros([training_size, 32]))

mlp2 = MLP(depth=3, width=32, activation='tanh')
mlp2.set_training_data(inputs=np.zeros([training_size, 32]), outputs=np.zeros([training_size, 16]))

mlp3 = MLP(depth=2, width=16, activation='tanh', linear_output=True)
mlp3.set_training_data(inputs=np.zeros([training_size, 16]), outputs=training_outputs)

print(mlp1._net)
print(mlp2._net)
print(mlp3._net)

# A mean squared error loss primitive
loss = Loss(loss='mse', target_outputs=training_outputs)

# End-to-end fine tuning of three primitives
print('End-to-end training starting')
end_to_end_iterations = 100
for i in range(end_to_end_iterations):
    # Forward
    o1 = mlp1.produce(inputs=training_inputs)
    o2 = mlp2.produce(inputs=o1)
    o3 = mlp3.produce(inputs=o2)

    # Error
    mse = loss.produce(actual_outputs=o3)
    print(i, mse)

    # Backpropagation and fine tuning
    grad_loss_o3, _ = loss.backprop()
    grad_o3_o2,   _ = mlp3.backprop(gradient_outputs=grad_loss_o3, fine_tune=True, learning_rate=0.001)
    grad_o2_o1,   _ = mlp2.backprop(gradient_outputs=grad_o3_o2, fine_tune=True, learning_rate=0.001)
    _,            _ = mlp1.backprop(gradient_outputs=grad_o2_o1, fine_tune=True, learning_rate=0.001)
print('End-to-end training finished')

# Test the pipeline of three primitives trained end-to-end
outputs = mlp3.produce(inputs=mlp2.produce(inputs=mlp1.produce(inputs=training_inputs))).reshape(-1)
mse = loss.produce(actual_outputs=outputs)
print('MSE: ', mse)
