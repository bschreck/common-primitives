from typing import NamedTuple

from sklearn.linear_model import BayesianRidge as BRR  # type: ignore

from d3m_metadata.sequence import ndarray
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase

# These are just regular Python variables so that we can easily change all types
# at once in the future, if needed. Otherwise, one could simply inline all these.
Inputs = ndarray
Outputs = ndarray

# A named tuple for parameters.
# Specifying types for all parameters is important so that one can do end-to-end
# training from outside. For example, some parameters might have gradients so we
# can use those to optimize them end-to-end.
Params = NamedTuple('Params', [
    ('coefficient', ndarray),
])


class BayesianRidge(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params]):
    """
    Primitive wrapping for sklearn.linear_model.BayesianRidge.
    """

    __author__ = "NYU DARPA D3M team"
    __metadata__ = {
        "common_name": "Bayesian Ridge Regression",
        "algorithm_type": ["Bayesian", "Regression"],
        "compute_resources": {
            "sample_size": [],
            "sample_unit": [],
            "disk_per_node": [],
            "expected_running_time": [],
            "gpus_per_node": [],
            "cores_per_node": [],
            "mem_per_gpu": [],
            "mem_per_node": [],
            "num_nodes": []
        },
        "handles_regression": True,
        "handles_classification": False,
        "handles_multiclass": False,
        "handles_multilabel": False,
        "input_type": ["DENSE", "SPARSE"],
        "output_type": ["PREDICTIONS"]
    }

    # It is important that all hyper-parameters (parameters which do not change during
    # a life-cycle of a primitive) are explicitly listed and typed in the constructor.
    # This allows one to do hyper-parameter tuning and explores the space of
    # hyper-parameters.
    def __init__(self, *,
                 n_iter: int = 300,
                 tol: float = 0.001,
                 alpha_1: float = 1e-06,
                 alpha_2: float = 1e-06,
                 lambda_1: float = 1e-06,
                 lambda_2: float = 1e-06,
                 compute_score: bool = False,
                 fit_intercept: bool = True,
                 normalize: bool = False,
                 _verbose: bool = False) -> None:

        super().__init__()

        self.bayesian_ridge = BRR(
            n_iter=n_iter,
            tol=tol,
            alpha_1=alpha_1,
            alpha_2=alpha_2,
            lambda_1=lambda_1,
            lambda_2=lambda_2,
            compute_score=compute_score,
            fit_intercept=fit_intercept,
            normalize=normalize,
            copy_X=True,
            verbose=_verbose)

        self.training_inputs = None  # type: ndarray
        self.training_outputs = None  # type: ndarray
        self.fitted = False

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> Outputs:
        return self.bayesian_ridge.predict(inputs)

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self.training_inputs = inputs
        self.training_outputs = outputs
        self.fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> None:
        # If already fitted with current training data, this call is a noop.
        if self.fitted:
            return

        if self.training_inputs is None or self.training_outputs is None:
            raise ValueError("Missing training data.")

        self.bayesian_ridge.fit(self.training_inputs, self.training_outputs)
        self.fitted = True

    def get_params(self) -> Params:
        return Params(coefficient=self.bayesian_ridge.coef_)

    def set_params(self, *, params: Params) -> None:
        self.bayesian_ridge.coef_ = params.coefficient
