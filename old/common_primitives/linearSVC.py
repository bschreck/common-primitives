from typing import NamedTuple

from sklearn.svm import LinearSVC as LSVC  # type: ignore

from d3m_metadata.sequence import ndarray
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase

# These are just regular Python variables so that we can easily change all types
# at once in the future, if needed. Otherwise, one could simply inline all these.
Inputs = ndarray
Outputs = ndarray

# A named tuple for parameters.
# Specifying types for all parameters is important so that one can do end-to-end
# training from outside. For example, some parameters might have gradients so we
# can use those to optimize them end-to-end.
Params = NamedTuple('Params', [
    ('coefficient', ndarray),
])


class LinearSVC(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params]):
    """
    Primitive wrapping for sklearn.svm.LinearSVC.
    """

    __author__ = "NYU DARPA D3M team"
    __metadata__ = {
        "common_name": "Linear Support Vector Classification",
        "algorithm_type": ["Classification"],
        "compute_resources": {
            "sample_size": [],
            "sample_unit": [],
            "disk_per_node": [],
            "expected_running_time": [],
            "gpus_per_node": [],
            "cores_per_node": [],
            "mem_per_gpu": [],
            "mem_per_node": [],
            "num_nodes": []
        },
        "handles_regression": False,
        "handles_classification": True,
        "handles_multiclass": True,
        "handles_multilabel": False,
        "input_type": ["DENSE", "SPARSE"],
        "output_type": ["PREDICTIONS"]
    }

    # It is important that all hyper-parameters (parameters which do not change during
    # a life-cycle of a primitive) are explicitly listed and typed in the constructor.
    # This allows one to do hyper-parameter tuning and explores the space of
    # hyper-parameters.
    def __init__(self, *,
                 penalty: str = 'l2',
                 loss: str = 'squared_hinge',
                 dual: bool = True,
                 tol: float = 0.0001,
                 C: float = 1.0,
                 multi_class: str ='ovr',
                 fit_intercept: bool=True,
                 intercept_scaling: float = 1,
                 class_weight: dict = None,
                 _verbose: int = 0,
                 max_iter: int = 1000) -> None:

        super().__init__()

        self.linear_svc = LSVC(
            penalty=penalty,
            loss=loss,
            dual=dual,
            tol=tol,
            C=C,
            multi_class=multi_class,
            fit_intercept=fit_intercept,
            intercept_scaling=intercept_scaling,
            class_weight=class_weight,
            verbose=_verbose,
            max_iter=max_iter)

        self.training_inputs = None  # type: ndarray
        self.training_outputs = None  # type: ndarray
        self.fitted = False

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> Outputs:
        return self.linear_svc.predict(inputs)

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self.training_inputs = inputs
        self.training_outputs = outputs
        self.fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> None:
        # If already fitted with current training data, this call is a noop.
        if self.fitted:
            return

        if self.training_inputs is None or self.training_outputs is None:
            raise ValueError("Missing training data.")

        self.linear_svc.fit(self.training_inputs, self.training_outputs)
        self.fitted = True

    def get_params(self) -> Params:
        return Params(coefficient=self.linear_svc.coef_)

    def set_params(self, *, params: Params) -> None:
        self.linear_svc.coef_ = params.coefficient
