from typing import NamedTuple, Sequence, Any

from sklearn.tree import DecisionTreeClassifier  # type: ignore
from sklearn.tree._tree import Tree  # type: ignore

from d3m_metadata.sequence import ndarray
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase

# These are just regular Python variables so that we can easily change all types
# at once in the future, if needed. Otherwise, one could simply inline all these.
Inputs = ndarray
Outputs = Sequence[Any]

# A named tuple for parameters.
# Specifying types for all parameters is important so that one can do end-to-end
# training from outside. For example, some parameters might have gradients so we
# can use those to optimize them end-to-end.
Params = NamedTuple('Params', [
    ('tree', Tree),
])


class DecisionTree(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params]):
    """
    Primitive wrapping sklearn.tree.DecisionTreeClassifier.
    """

    __author__ = "NYU DARPA D3M team"
    __metadata__ = {
        "common_name": "Decision Tree",
        "algorithm_type": ["Decision Tree", "Classification"],
        "compute_resources": {
            "sample_size": [],
            "sample_unit": [],
            "disk_per_node": [],
            "expected_running_time": [],
            "gpus_per_node": [],
            "cores_per_node": [],
            "mem_per_gpu": [],
            "mem_per_node": [],
            "num_nodes": []
        },
        "handles_regression": False,
        "handles_classification": True,
        "handles_multiclass": True,
        "handles_multilabel": True,
        "input_type": ["DENSE", "UNISGNED_DATA"],
        "output_type": ["PREDICTIONS"]
    }

    # It is important that all hyper-parameters (parameters which do not change during
    # a life-cycle of a primitive) are explicitly listed and typed in the constructor.
    # This allows one to do hyper-parameter tuning and explores the space of
    # hyper-parameters.
    def __init__(self, *,
                 criterion: str = 'gini',
                 splitter: str = 'best',
                 max_depth: int = None,
                 min_samples_split: int = 2,
                 min_samples_leaf: int = 1,
                 min_weight_fraction_leaf: float = 0.0,
                 max_features: int = None,
                 max_leaf_nodes: int = None,
                 min_impurity_decrease: float = 0.0,
                 min_impurity_split: float = None,
                 class_weight: dict = None,
                 presort: bool = False) -> None:

        super().__init__()

        self.decision_tree = DecisionTreeClassifier(
            criterion=criterion,
            splitter=splitter,
            max_depth=max_depth,
            min_samples_split=min_samples_split,
            min_samples_leaf=min_samples_leaf,
            min_weight_fraction_leaf=min_weight_fraction_leaf,
            max_features=max_features,
            max_leaf_nodes=max_leaf_nodes,
            min_impurity_decrease=min_impurity_decrease,
            min_impurity_split=min_impurity_split,
            class_weight=class_weight,
            presort=presort)

        self.training_inputs = None  # type: ndarray
        self.training_outputs = None  # type: Sequence[Any]
        self.fitted = False

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> Outputs:
        return self.decision_tree.predict(inputs)

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self.training_inputs = inputs
        self.training_outputs = outputs
        self.fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> None:
        # If already fitted with current training data, this call is a noop.
        if self.fitted:
            return

        if self.training_inputs is None or self.training_outputs is None:
            raise ValueError("Missing training data.")

        self.decision_tree.fit(self.training_inputs, self.training_outputs)
        self.fitted = True

    def get_params(self) -> Params:
        return Params(tree=self.decision_tree.tree_)

    def set_params(self, *, params: Params) -> None:
        self.decision_tree.tree_ = params.tree
