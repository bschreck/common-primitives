from functools import reduce
from numbers import Number
import time
from typing import Sequence, Any, NamedTuple, Dict, List

import numpy as np  # type: ignore

from d3m_metadata.sequence import ndarray
from primitive_interfaces.unsupervised_learning import UnsupervisedLearnerPrimitiveBase

# dict where each value is a list containing an unknown data type
Inputs = Sequence[Dict[Any, List[Any]]]
# same as input but with each discrete data type converted to a one hot encoding and data vector for each key concatenated
Outputs = ndarray
# each key has a vector such that the encoding for vector[2] is [0 0 1 0 0 0 ...]
Params = NamedTuple('Params', [
    ('categories', Dict[Any, List[Any]]),
])


def get_categories(discrete_list):
    values = []
    for value in discrete_list:
        if value not in values:
            values.append(value)
    return values


class OneHotMaker(UnsupervisedLearnerPrimitiveBase[Inputs, Outputs, Params]):
    """
    Attempts to detect discrete values in data and convert these to a
    one-hot embedding.
    """

    __author__ = "William Harvey <willh@robots.ox.ac.uk>"
    __metadata__ = {
        "team": "Oxford DARPA D3M \"Hasty\" team",
        "common_name": "One Hot Maker",
        "task_type": ["Data Processing"],
        "compute_resources": {
            "sample_size": [],
            "sample_unit": [],
            "disk_per_node": [],
            "expected_running_time": [],
            "gpus_per_node": [],
            "cores_per_node": [],
            "mem_per_gpu": [],
            "mem_per_node": [],
            "num_nodes": [],
        },
        "handles_regression": False,
        "handles_classification": False,
        "handles_multiclass": False,
        "handles_multilabel": False,
        "algorithm_type": ["Data Processing"]
    }

    def __init__(self, cutoff_for_categorical=0.05):
        super().__init__()
        self.cutoff_for_categorical = cutoff_for_categorical
        self.training_input = {}

    def set_training_data(self, *, inputs: Inputs) -> None:
        """ All inputs should have the same keys - the lists for each key are then concatenated """
        self.training_input = {}
        for key in inputs[0]:
            self.training_input[key] = reduce(lambda a, b: a+b, [input[key] for input in inputs])

    def get_params(self) -> Params:
        return Params(categories=self.categories)

    def set_params(self, *, params: Params) -> None:
        self.categories = params.categories

    def fit(self, *, timeout: float = None, iterations: int = None) -> None:
        """ Fit based on provided training data.
        """
        start = time.time()
        if self.training_input is None:
            raise ValueError("Missing training data.")

        self.categories = {}
        for key in self.training_input:
            if timeout and time.time() > start + timeout:
                raise TimeoutError

            seen_values = []  # type: List[Any]
            for value in self.training_input[key]:
                if value not in seen_values:
                    seen_values.append(value)
            if len(seen_values) > self.cutoff_for_categorical * len(self.training_input[key]) and isinstance(seen_values[0], Number):    # try to infer whether or not the data is discrete
                self.categories[key] = None
            else:
                self.categories[key] = seen_values

    def produce(self, *, inputs: Inputs,
                timeout: float = None,
                iterations: int = None,
                allow_unseen_categories=True) -> Outputs:
        return np.array([self._produce_one(input=i,
                                           allow_unseen_categories=allow_unseen_categories)
                         for i in inputs])

    def _produce_one(self, *, input: dict,
                     allow_unseen_categories) -> ndarray:
        one_hot = {}
        for key in input:
            if self.categories[key] is None:
                one_hot[key] = input[key]
            else:
                # Do some transform using Params.
                one_hot[key] = [[0] * len(self.categories[key]) for _ in input[key]]
                for i in range(len(input[key])):  # iterate through each data point
                    try:
                        one_hot[key][i][self.categories[key].index(input[key][i])] = 1
                    except ValueError:
                        if input[key][i] not in self.categories[key]:
                            if allow_unseen_categories:
                                pass
                            else:
                                raise ValueError("Value {} not encountered during fit".format(input[key][i]))

        # Now concatenate vector for each key:
        result_vectors = [[] for _ in next(iter(one_hot.values()))]  # type: List[List[Any]]
        for key in one_hot:
            for i in range(len(one_hot[key])):
                try:
                    result_vectors[i].extend(one_hot[key][i])
                except TypeError:  # if one_hot[key][i] is not a list
                    result_vectors[i].append(one_hot[key][i])
        return np.array(result_vectors)
