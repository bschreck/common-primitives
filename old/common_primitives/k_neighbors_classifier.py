from typing import NamedTuple

from sklearn.neighbors import KNeighborsClassifier as KNC  # type: ignore

from d3m_metadata.sequence import ndarray
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase

# These are just regular Python variables so that we can easily change all types
# at once in the future, if needed. Otherwise, one could simply inline all these.
Inputs = ndarray
Outputs = ndarray

# A named tuple for parameters.
# Specifying types for all parameters is important so that one can do end-to-end
# training from outside. For example, some parameters might have gradients so we
# can use those to optimize them end-to-end.
Params = NamedTuple('Params', [
    ('instances', ndarray),
    ('targets', ndarray),
])


class KNeighborsClassifier(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params]):
    """
    Primitive wrapping for sklearn.neighbors.KNeigborsClassifier.
    """

    __author__ = "NYU DARPA D3M team"
    __metadata__ = {
        "common_name": "K Neighbors Classification",
        "algorithm_type": ["Instance Based", "Classification"],
        "compute_resources": {
            "sample_size": [],
            "sample_unit": [],
            "disk_per_node": [],
            "expected_running_time": [],
            "gpus_per_node": [],
            "cores_per_node": [],
            "mem_per_gpu": [],
            "mem_per_node": [],
            "num_nodes": []
        },
        "handles_regression": False,
        "handles_classification": True,
        "handles_multiclass": True,
        "handles_multilabel": False,
        "input_type": ["DENSE", "SPARSE"],
        "output_type": ["PREDICTIONS"]
    }

    # It is important that all hyper-parameters (parameters which do not change during
    # a life-cycle of a primitive) are explicitly listed and typed in the constructor.
    # This allows one to do hyper-parameter tuning and explores the space of
    # hyper-parameters.
    def __init__(self, *,
                 n_neighbors: int = 5,
                 weights: str = 'uniform',
                 algorithm: str = 'auto',
                 leaf_size: int = 30,
                 p: int = 2,
                 metric: str = 'minkowski',
                 metric_params: dict = None) -> None:

        super().__init__()

        self.knc = KNC(
            n_neighbors=n_neighbors,
            weights=weights,
            algorithm=algorithm,
            leaf_size=leaf_size,
            p=p,
            metric=metric,
            metric_params=metric_params)

        self.training_inputs = None  # type: ndarray
        self.training_outputs = None  # type: ndarray
        self.fitted = False

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> Outputs:
        return self.knc.predict(inputs)

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self.training_inputs = inputs
        self.training_outputs = outputs
        self.fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> None:
        # If already fitted with current training data, this call is a noop.
        if self.fitted:
            return

        if self.training_inputs is None or self.training_outputs is None:
            raise ValueError("Missing training data.")

        self.knc.fit(self.training_inputs, self.training_outputs)
        self.fitted = True

    def get_params(self) -> Params:
        return Params(instances=self.training_inputs, targets=self.training_outputs)

    def set_params(self, *, params: Params) -> None:
        self.training_inputs = Params.instances
        self.training_outputs = Params.targets
