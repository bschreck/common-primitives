from typing import NamedTuple

from sklearn.linear_model import Ridge as RR  # type: ignore

from d3m_metadata.sequence import ndarray
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase

# These are just regular Python variables so that we can easily change all types
# at once in the future, if needed. Otherwise, one could simply inline all these.
Inputs = ndarray
Outputs = ndarray

# A named tuple for parameters.
# Specifying types for all parameters is important so that one can do end-to-end
# training from outside. For example, some parameters might have gradients so we
# can use those to optimize them end-to-end.
Params = NamedTuple('Params', [
    ('coefficient', ndarray),
])


class Ridge(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params]):
    """
    Primitive wrapping for sklearn.linear_model.Ridge.
    """

    __author__ = "NYU DARPA D3M team"
    __metadata__ = {
        "common_name": "Ridge Regression",
        "algorithm_type": ["Regression"],
        "compute_resources": {
            "sample_size": [],
            "sample_unit": [],
            "disk_per_node": [],
            "expected_running_time": [],
            "gpus_per_node": [],
            "cores_per_node": [],
            "mem_per_gpu": [],
            "mem_per_node": [],
            "num_nodes": []
        },
        "handles_regression": True,
        "handles_classification": False,
        "handles_multiclass": False,
        "handles_multilabel": False,
        "input_type": ["DENSE", "SPARSE"],
        "output_type": ["PREDICTIONS"]
    }

    # It is important that all hyper-parameters (parameters which do not change during
    # a life-cycle of a primitive) are explicitly listed and typed in the constructor.
    # This allows one to do hyper-parameter tuning and explores the space of
    # hyper-parameters.
    def __init__(self, *,
                 alpha: float = 1.0,
                 fit_intercept: bool = True,
                 normalize: bool = False,
                 max_iter: int = None,
                 tol: float = 0.001,
                 solver: str = 'auto') -> None:

        super().__init__()

        self.ridge = RR(
            alpha=alpha,
            fit_intercept=fit_intercept,
            normalize=normalize,
            copy_X=True,
            max_iter=max_iter,
            tol=tol,
            solver=solver)
        self.training_inputs = None  # type: ndarray
        self.training_outputs = None  # type: ndarray
        self.fitted = False

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> Outputs:
        return self.ridge.predict(inputs)

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self.training_inputs = inputs
        self.training_outputs = outputs
        self.fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> None:
        # If already fitted with current training data, this call is a noop.
        if self.fitted:
            return

        if self.training_inputs is None or self.training_outputs is None:
            raise ValueError("Missing training data.")

        self.ridge.fit(self.training_inputs, self.training_outputs)
        self.fitted = True

    def get_params(self) -> Params:
        return Params(coefficient=self.ridge.coef_)

    def set_params(self, *, params: Params) -> None:
        self.ridge.coef_ = params.coefficient
