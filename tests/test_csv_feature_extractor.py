import unittest
import os
import pandas as pd
from pandas.util.testing import assert_frame_equal

from common_primitives.csv_feature_extractor import CSVFeatureExtractor, Hyperparams


class CSVFeatureExtractorTestCase(unittest.TestCase):
    def test_produce(self):
        csv = CSVFeatureExtractor(hyperparams=Hyperparams.defaults())
        test_file = os.path.dirname(os.path.realpath(__file__)) + "/test_csv/test.csv"

        data = csv.produce(inputs=[test_file]).value
        ground_truth = pd.DataFrame({'word': ['one', 'two', 'three'],
                           'number': [1, 2, 3],
                           'value': [1.0, 2.0, 3.0]})

        assert_frame_equal(ground_truth, data)


if __name__ == '__main__':
    unittest.main()
