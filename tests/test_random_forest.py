import unittest

import numpy as np

from common_primitives.random_forest import RandomForestClassifier, Hyperparams


class RandomForestTestCase(unittest.TestCase):
    def test_fit(self):
        clf = RandomForestClassifier(hyperparams=Hyperparams.defaults())
        train_y = np.array(['a', 'b', 'a', 'b', 'a'])
        train_x = np.array([[2, 8], [3.5, 3], [7, 2], [8, 1], [9, 13]])
        clf.set_training_data(inputs=train_x, outputs=train_y)
        clf.fit()

        assert np.array_equal(clf.produce(inputs=[np.array([3, 3]), np.array([13, 13])]).value,
                              clf.produce(inputs=np.array([[3, 3], [13, 13]])).value)


if __name__ == '__main__':
    unittest.main()
