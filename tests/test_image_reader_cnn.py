import unittest
import os
import numpy as np
from common_primitives import image_reader, convolutional_neural_net


class ImageReaderCNNTestCase(unittest.TestCase):
    def test_mnist_accuracy(self):
        ir = image_reader.ImageReader(hyperparams=image_reader.Hyperparams.defaults())

        test_dir = os.path.dirname(os.path.realpath(__file__)) + "/test_image"
        mnist_train_files = ['mnist_0_1.png',
                             'mnist_0_2.png',
                             'mnist_0_3.png',
                             'mnist_0_4.png',
                             'mnist_0_5.png',
                             'mnist_1_1.png',
                             'mnist_1_2.png',
                             'mnist_1_3.png',
                             'mnist_1_4.png',
                             'mnist_1_5.png']
        mnist_train_files = ['{}/{}'.format(test_dir, i) for i in mnist_train_files]

        mnist_train_inputs = ir.produce(inputs=mnist_train_files).value
        mnist_train_outputs = np.array([0,0,0,0,0,1,1,1,1,1])

        hy = convolutional_neural_net.Hyperparams(convolutional_neural_net.Hyperparams.defaults(), family='lenet', output_dim=2, loss_type='crossentropy', fit_threshold=1e-3)
        cnn = convolutional_neural_net.ConvolutionalNeuralNet(hyperparams=hy)

        cnn.set_training_data(inputs=mnist_train_inputs, outputs=mnist_train_outputs)
        cnn.fit(iterations=1000)
        outputs = cnn.produce(inputs=mnist_train_inputs).value
        outputs = np.argmax(outputs, 1)
        accuracy = np.sum(outputs == mnist_train_outputs) / len(outputs)
        self.assertTrue(accuracy >= 0.9)

    def test_cifar10_accuracy(self):
        ir = image_reader.ImageReader(hyperparams=image_reader.Hyperparams.defaults())

        test_dir = os.path.dirname(os.path.realpath(__file__)) + "/test_image"
        mnist_train_files = ['cifar10_bird_1.png',
                             'cifar10_bird_2.png',
                             'cifar10_bird_3.png',
                             'cifar10_bird_4.png',
                             'cifar10_bird_5.png',
                             'cifar10_ship_1.png',
                             'cifar10_ship_2.png',
                             'cifar10_ship_3.png',
                             'cifar10_ship_4.png',
                             'cifar10_ship_5.png']
        mnist_train_files = ['{}/{}'.format(test_dir, i) for i in mnist_train_files]

        mnist_train_inputs = ir.produce(inputs=mnist_train_files).value
        mnist_train_outputs = np.array([0,0,0,0,0,1,1,1,1,1])

        hy = convolutional_neural_net.Hyperparams(convolutional_neural_net.Hyperparams.defaults(), family='lenet', output_dim=2, loss_type='crossentropy', fit_threshold=1e-3)
        cnn = convolutional_neural_net.ConvolutionalNeuralNet(hyperparams=hy)

        cnn.set_training_data(inputs=mnist_train_inputs, outputs=mnist_train_outputs)
        cnn.fit(iterations=1000)
        outputs = cnn.produce(inputs=mnist_train_inputs).value
        outputs = np.argmax(outputs, 1)
        accuracy = np.sum(outputs == mnist_train_outputs) / len(outputs)
        self.assertTrue(accuracy >= 0.9)

if __name__ == '__main__':
    unittest.main()
