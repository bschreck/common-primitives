import os
import torchvision.datasets as datasets
import torchvision.transforms as transforms


def get_mnist_train_data(size):
    mnist_root = os.path.dirname(os.path.realpath(__file__)) + '/test_data_mnist'
    download_if_missing = True
    trans = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5,), (1.0,))])
    mnist_train = datasets.MNIST(root=mnist_root, train=True, transform=trans, download=download_if_missing)
    # mnist_test = datasets.MNIST(root=mnist_root, train=False, transform=trans)
    mnist_train_inputs = mnist_train.train_data[:size].numpy()
    mnist_train_outputs = mnist_train.train_labels[:size].numpy()
    return mnist_train_inputs, mnist_train_outputs
